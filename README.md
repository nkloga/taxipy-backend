# Taxipy

## Links
- [Front-end project repository](https://bitbucket.org/nkloga/taxipy-frontend)
- [List of notes in Google Doc](https://docs.google.com/document/d/1fpuYT4JEYuOzSWR12CsNUkZungH8Anr7QkYMlU8paV8/edit?usp=sharing)
- [Trello](https://trello.com/b/Sio3k7sF/sda-taxipy)

## API documentation
run the app and check http://localhost:8080/swagger-ui.html