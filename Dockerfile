FROM java:8-jdk-alpine
COPY ./build/libs/TaxipyPractical-1.0-SNAPSHOT.war /usr/app/
WORKDIR /usr/app
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "TaxipyPractical-1.0-SNAPSHOT.war"]


