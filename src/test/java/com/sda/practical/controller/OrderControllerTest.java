package com.sda.practical.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sda.practical.dto.LostItemDto;
import com.sda.practical.dto.OrderDto;
import com.sda.practical.exception.ExceptionText;
import com.sda.practical.model.utils.LostItemType;
import com.sda.practical.model.utils.OrderStatus;
import com.sda.practical.security.JwtTokenUtil;
import com.sda.practical.service.utlis.SecurityContextUpdate;
import com.sda.practical.service.utlis.TestRoles;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Optional;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(scripts = "/test-dataset.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class OrderControllerTest {

    private MockMvc mvc;

    private final String bearer = "Bearer 5d1103e-b3e1-4ae9-b606-46c9c1bc915a";

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private AuthenticationManager authenticationManager;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    @Before
    public void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    private ObjectMapper configureObjectMapper() {
        ObjectMapper om = new ObjectMapper();
        om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return om;
    }

    private ArrayList<OrderDto> parseJsonOrderList(ResultActions orders) throws IOException {
        String data = orders.andReturn().getResponse().getContentAsString();
        ObjectMapper om = configureObjectMapper();
        JsonNode jsNode = om.readTree(data);
        String test = jsNode.at("/_embedded/orderDtoList").toString();
        return om.readValue(test, new TypeReference<List<OrderDto>>() {
        });
    }

    private OrderDto parseJsonOrder(ResultActions orders) throws IOException {
        String data = orders.andReturn().getResponse().getContentAsString();
        return configureObjectMapper().readValue(data, new TypeReference<OrderDto>() {
        });
    }

    @Test
    public void getOpenOrdersSuccess() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER);
        ResultActions orders = mvc.perform(get("/orders/open").header("Authorization", bearer)).andExpect(status().is2xxSuccessful());
        ArrayList<OrderDto> ordersList = parseJsonOrderList(orders);
        assertEquals(4, ordersList.size());
    }

    @Test
    public void getOrdersHistorySuccess() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER);
        ResultActions orders = mvc.perform(get("/orders/history").header("Authorization", bearer)).andExpect(status().is2xxSuccessful());
        ArrayList<OrderDto> ordersList = parseJsonOrderList(orders);
        assertEquals(9, ordersList.size());
    }

    @Test
    public void getLastOpenOrderSuccess() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER_WITH_ONE_ORDER);
        ResultActions orders = mvc.perform(get("/orders/last-open").header("Authorization", bearer)).andExpect(status().is2xxSuccessful());
        OrderDto order = parseJsonOrder(orders);
        assertEquals(Optional.of(21L), Optional.of(order.getOrderId()));
    }

    @Test
    public void getOrderByIdSuccess() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER_WITH_ONE_ORDER);
        ResultActions orders = mvc.perform(get("/orders/21").header("Authorization", bearer)).andExpect(status().is2xxSuccessful());
        OrderDto order = parseJsonOrder(orders);
        assertEquals(Optional.of(21L), Optional.of(order.getOrderId()));
    }

    @Test
    public void getOrderByIdFail() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER_WITH_ONE_ORDER);
        ResultActions order = mvc.perform(get("/orders/0").header("Authorization", bearer)).andExpect(status().is4xxClientError());
        assertEquals(order.andReturn().getResponse().getContentAsString(), ExceptionText.ORDER_NOT_FOUND.url());
    }

    @Test
    public void addOrderSuccess() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER_WITH_ONE_ORDER);
        OrderDto orderDto = new OrderDto();
        orderDto.setStatus(OrderStatus.IN_PROGRESS);
        orderDto.setFromLat("123456");
        orderDto.setFromLong("123456");
        orderDto.setWhereLat("123456");
        orderDto.setWhereLong("123456");

        LostItemDto lostItemDto = new LostItemDto();
        Long orderId = 36L;
        lostItemDto.setOrder(orderId);
        lostItemDto.setType(LostItemType.LOST);
        lostItemDto.setDescription("some things");


        ResultActions addedOrder = mvc.perform(post("/orders/add").header("Authorization", bearer).contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(orderDto)));
        ResultActions item = mvc.perform(post("/lost/submit").header("Authorization", bearer).contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(lostItemDto)));
        OrderDto savedOrderParsed = parseJsonOrder(addedOrder);
        Assert.assertNotNull(savedOrderParsed.getOrderId());
        assertEquals(savedOrderParsed.getFromLat(), orderDto.getFromLat());
        assertEquals(savedOrderParsed.getFromLong(), orderDto.getFromLong());
        assertEquals(savedOrderParsed.getWhereLat(), orderDto.getWhereLat());
        assertEquals(savedOrderParsed.getWhereLong(), orderDto.getWhereLong());
    }

    @Test
    public void addOrderNoCoordinateFail() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER_WITH_ONE_ORDER);
        OrderDto orderDto = new OrderDto();
        ResultActions resultActions = mvc.perform(post("/orders/add").header("Authorization", bearer).contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(orderDto)));
        assertEquals(resultActions.andReturn().getResponse().getContentAsString(), ExceptionText.ORDER_INVALID.url());
    }

    @Test
    public void cancelOrderSuccess() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        ResultActions result = mvc.perform(get("/orders/16/cancel").header("Authorization", bearer)).andExpect(status().is2xxSuccessful());
        OrderDto order = parseJsonOrder(result);
        assertEquals(order.getStatus(), OrderStatus.CANCELLED);
    }

    @Test
    public void cancelOrderFail() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER_WITH_ONE_ORDER);
        ResultActions result = mvc.perform(get("/orders/16/cancel").header("Authorization", bearer)).andExpect(status().is4xxClientError());
        assertEquals(result.andReturn().getResponse().getContentAsString(), ExceptionText.ORDER_HAS_NO_USER.url());
    }

    @Test
    public void startOrderSuccess() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER);
        ResultActions result = mvc.perform(get("/orders/22/start").header("Authorization", bearer));
        OrderDto order = parseJsonOrder(result);
        System.out.println(order);
        assertEquals(order.getStatus(), OrderStatus.STARTED);
    }

    @Test
    public void startOrderFail() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER_WITH_ONE_ORDER);
        ResultActions result = mvc.perform(get("/orders/22/start").header("Authorization", bearer)).andExpect(status().is4xxClientError());
        assertEquals(result.andReturn().getResponse().getContentAsString(), ExceptionText.ORDER_HAS_NO_USER.url());
    }

    @Test
    public void terminateOrderSuccess() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER);
        ResultActions result = mvc.perform(get("/orders/18/terminate").header("Authorization", bearer)).andExpect(status().is2xxSuccessful());
        OrderDto order = parseJsonOrder(result);
        assertEquals(order.getStatus(), OrderStatus.TERMINATED);
    }

    @Test
    public void terminateOrderFail() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER_WITH_ONE_ORDER);
        ResultActions result = mvc.perform(get("/orders/18/terminate").header("Authorization", bearer)).andExpect(status().is4xxClientError());
        assertEquals(result.andReturn().getResponse().getContentAsString(), ExceptionText.ORDER_HAS_NO_USER.url());
    }

    @Test
    public void payOrderSuccess() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        ResultActions result = mvc.perform(get("/orders/20/pay").header("Authorization", bearer)).andExpect(status().is2xxSuccessful());
        OrderDto order = parseJsonOrder(result);
        assertEquals(order.getStatus(), OrderStatus.PAID);
    }

    @Test
    public void payOrderFail() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER_WITH_ONE_ORDER);
        mvc.perform(get("/orders/20/pay").header("Authorization", bearer)).andExpect(status().is4xxClientError());
    }

    @Test
    public void completeOrderSuccess() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER);
        ResultActions result = mvc.perform(get("/orders/17/complete").header("Authorization", bearer)).andExpect(status().is2xxSuccessful());
        OrderDto order = parseJsonOrder(result);
        assertEquals(order.getStatus(), OrderStatus.COMPLETED);
    }

    @Test
    public void completeOrderFail() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER_WITH_ONE_ORDER);
        mvc.perform(get("/orders/17/complete").header("Authorization", bearer)).andExpect(status().is4xxClientError());
    }

    @Test
    public void acceptOrderSuccess() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER);
        ResultActions result = mvc.perform(get("/orders/15/accept").header("Authorization", bearer)).andExpect(status().is2xxSuccessful());
        OrderDto order = parseJsonOrder(result);
        assertEquals(order.getStatus(), OrderStatus.ACCEPTED);
    }

    @Test
    public void acceptOrderFail() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER_WITH_ONE_ORDER);
        ResultActions result = mvc.perform(get("/orders/17/accept").header("Authorization", bearer)).andExpect(status().is4xxClientError());
        Assert.assertTrue(result.andReturn().getResponse().getContentAsString().contains("Can't accept the order!"));
    }

    @Test
    public void feedbackOrderSuccess() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        ResultActions result = mvc.perform(post("/orders/33/feedback").header("Authorization", bearer).contentType(MediaType.APPLICATION_JSON).content("4")).andExpect(status().is2xxSuccessful());
        OrderDto order = parseJsonOrder(result);
        assertEquals(4, (int) order.getRating());
    }

    @Test
    public void feedbackOrderFail() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER_WITH_ONE_ORDER);
        mvc.perform(get("/orders/33/feedback").header("Authorization", bearer)).andExpect(status().is4xxClientError());
    }


}