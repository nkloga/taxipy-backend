package com.sda.practical.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sda.practical.dto.AuthUserDto;
import com.sda.practical.dto.OrderDto;
import com.sda.practical.exception.ExceptionText;
import com.sda.practical.model.utils.OrderStatus;
import com.sda.practical.security.JwtTokenUtil;
import com.sda.practical.service.utlis.SecurityContextUpdate;
import com.sda.practical.service.utlis.TestRoles;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(scripts = "/test-dataset.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class AdminControllerTest {

    private MockMvc mvc;

    private final String bearer = "Bearer 5d1103e-b3e1-4ae9-b606-46c9c1bc915a";

    @Autowired
    private WebApplicationContext context;


    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    @Before
    public void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    private ObjectMapper configureObjectMapper() {
        ObjectMapper om = new ObjectMapper();
        om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return om;
    }

    private ArrayList<OrderDto> parseJsonList(ResultActions users) throws IOException {
        String data = users.andReturn().getResponse().getContentAsString();
        ObjectMapper om = configureObjectMapper();
        JsonNode jsNode = om.readTree(data);
        String test = jsNode.at("").toString();
        return om.readValue(test, new TypeReference<List<AuthUserDto>>() {
        });
    }

    private AuthUserDto parseJson(ResultActions user) throws IOException {
        String data = user.andReturn().getResponse().getContentAsString();
        return configureObjectMapper().readValue(data, new TypeReference<AuthUserDto>() {
        });
    }

    @Test
    public void blockUserSuccess() throws Exception {
        Long userId = 2L;
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.ADMIN);
        ResultActions user = mvc.perform(get("/admin/block-user/" + userId).header("Authorization", bearer)).andExpect(status().is2xxSuccessful());
        AuthUserDto authUser = parseJson(user);
        Assert.assertEquals(userId, authUser.getId());
        Assert.assertFalse(authUser.isEnabled());
    }

    @Test
    public void blockUserFail() throws Exception {
        Long userId = 2L;
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER);
        mvc.perform(get("/admin/block-user/" + userId).header("Authorization", bearer)).andExpect(status().is4xxClientError());
    }

    @Test
    public void unblockUserSuccess() throws Exception {
        Long userId = 36L;
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.ADMIN);
        ResultActions userUnblock = mvc.perform(get("/admin/unblock-user/" + userId).header("Authorization", bearer)).andExpect(status().is2xxSuccessful());
        AuthUserDto authUserUnblock = parseJson(userUnblock);
        Assert.assertEquals(userId, authUserUnblock.getId());
        Assert.assertTrue(authUserUnblock.isEnabled());
    }

    @Test
    public void unblockUserFail() throws Exception {
        Long userId = 35L;
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER);
        ResultActions user = mvc.perform(get("/admin/unblock-user/" + userId).header("Authorization", bearer)).andExpect(status().is4xxClientError());
    }

    @Test
    public void getListOfUsersSuccess() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.ADMIN);
        ResultActions user = mvc.perform(get("/admin/list").header("Authorization", bearer)).andExpect(status().is2xxSuccessful());
        ArrayList<OrderDto> ordersList = parseJsonList(user);
        Assert.assertEquals(12, ordersList.size());
    }
}