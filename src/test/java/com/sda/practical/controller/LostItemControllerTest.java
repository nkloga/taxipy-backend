package com.sda.practical.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sda.practical.dto.LostItemDto;
import com.sda.practical.model.utils.LostItemStatus;
import com.sda.practical.security.JwtTokenUtil;
import com.sda.practical.service.utlis.SecurityContextUpdate;
import com.sda.practical.service.utlis.TestRoles;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(scripts = "/test-dataset.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class LostItemControllerTest {

    private MockMvc mvc;

    private final String bearer = "Bearer 5d1103e-b3e1-4ae9-b606-46c9c1bc915a";

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    @Before
    public void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    private ObjectMapper configureObjectMapper() {
        ObjectMapper om = new ObjectMapper();
        om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return om;
    }

    private ArrayList<LostItemDto> parseJsonList(ResultActions users) throws IOException {
        String data = users.andReturn().getResponse().getContentAsString();
        ObjectMapper om = configureObjectMapper();
        JsonNode jsNode = om.readTree(data);
        String test = jsNode.at("/_embedded/lostItemDtoList").toString();
        return om.readValue(test, new TypeReference<List<LostItemDto>>() {
        });
    }

    private LostItemDto parseJson(ResultActions user) throws IOException {
        String data = user.andReturn().getResponse().getContentAsString();
        return configureObjectMapper().readValue(data, new TypeReference<LostItemDto>() {
        });
    }

//    @Test TODO post request returns 415 or 400
//    public void submitLostItemSuccess() throws Exception {
//        LostItemDto lostItemDto = new LostItemDto();
//        Long orderId = 36L;
//        lostItemDto.setOrderId(orderId);
//        lostItemDto.setType(LostItemType.LOST);
//        lostItemDto.setDescription("some things");
//        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
//        ResultActions item = mvc.perform(post("/lost/submit").header("Authorization", bearer).contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(lostItemDto))).andExpect(status().is2xxSuccessful());
//        LostItemDto lostItem = parseJson(item);
//        Assert.assertEquals(orderId, lostItem.getOrderId());
//    }

    @Test
    public void closeItemCaseSuccess() throws Exception {
        Long id = 27L;
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.ADMIN);
        ResultActions item = mvc.perform(get("/lost/close/"+id).header("Authorization", bearer).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is2xxSuccessful());
        LostItemDto lostItem = parseJson(item);
        Assert.assertEquals(lostItem.getStatus(),LostItemStatus.CLOSED);
    }

    @Test
    public void getListOfItemsSuccess() throws Exception {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.ADMIN);
        ResultActions item = mvc.perform(get("/lost/list").header("Authorization", bearer).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is2xxSuccessful());
        List<LostItemDto> lostItem = parseJsonList(item);
        Assert.assertEquals(1, lostItem.size());
    }
}