package com.sda.practical.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sda.practical.dto.UserDto;
import com.sda.practical.exception.ExceptionText;
import com.sda.practical.model.User;
import com.sda.practical.model.utils.AuthorityName;
import com.sda.practical.security.JwtAuthenticationRequest;
import com.sda.practical.security.JwtTokenUtil;
import com.sda.practical.security.service.JwtUserDetailsService;
import com.sda.practical.service.utlis.SecurityContextUpdate;
import com.sda.practical.service.utlis.TestRoles;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static io.restassured.RestAssured.when;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(scripts = "/test-dataset.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class UserControllerTest {

    @Value("${jwt.route.authentication.path}")
    private String authPath;

    private MockMvc mvc;

    private final String bearer = "Bearer 5d1103e-b3e1-4ae9-b606-46c9c1bc915a";

    @Autowired
    private WebApplicationContext context;
//
//    @MockBean
//    private JwtTokenUtil jwtTokenUtil;
//
//    @MockBean
//    private JwtUserDetailsService jwtUserDetailsService;

    @Before
    public void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @Test
    public void createAuthenticationTokenSuccess() throws Exception {
        JwtAuthenticationRequest jwtAuthenticationRequest = new JwtAuthenticationRequest("test-user3", "123456");
        ResultActions result = mvc.perform(post(authPath).contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(jwtAuthenticationRequest)));
        Assert.assertTrue(result.andReturn().getResponse().getContentAsString().length() > 150);
    }

    @Test
    public void createAuthenticationTokenBadCredentialsFail() throws Exception {
        JwtAuthenticationRequest jwtAuthenticationRequest = new JwtAuthenticationRequest("test", "123456");
        ResultActions result = mvc.perform(post(authPath).contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(jwtAuthenticationRequest))).andExpect(status().isUnauthorized());
        Assert.assertEquals(result.andReturn().getResponse().getContentAsString(), ExceptionText.BAD_CREDENTIALS.url());
    }

    @Test
    public void createAuthenticationTokenDisabledUserFail() throws Exception {
        JwtAuthenticationRequest jwtAuthenticationRequest = new JwtAuthenticationRequest("test-user7", "123456");
        ResultActions result = mvc.perform(post(authPath).contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(jwtAuthenticationRequest))).andExpect(status().isUnauthorized());
        Assert.assertEquals(result.andReturn().getResponse().getContentAsString(), ExceptionText.USER_DISABLED.url());
    }

//    @Test
//    public void getAuthenticatedUserSuccess() throws Exception {
//        UserDto user = SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER);
//        Mockito.when(jwtTokenUtil.getUsernameFromToken(any())).thenReturn(user.getUsername());
//        Mockito.when(jwtUserDetailsService.loadUserByUsername(eq(user.getUsername()))).thenReturn(user);
//        Mockito.when(jwtTokenUtil.canTokenBeRefreshed(any(), any())).thenReturn(true);
//        mvc.perform(get("/user").header("Authorization", bearer)).andExpect(status().is2xxSuccessful());
//    }

//    @Test TODO post request returns 415 or 400
//    public void registerUserSuccess() throws Exception {
//        UserDto user = SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER);
//
//        User user2 = new User();
//        user2.setEmail("emaila");
//        user2.setUsername("testuser");
//        user2.setFirstname("userse");
//        user2.setLastname("dsdsda");
//        user2.setPassword("123123123");
//        user2.setEnabled(true);
//        user2.setPhoneNumber("443434343");
//
//        ResultActions resultActions = mvc.perform(post("/user/register").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(user2)));
//        System.out.println(resultActions);}
}