package com.sda.practical.ui.pageobjects;

import com.codeborne.selenide.SelenideDriver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import java.text.DecimalFormat;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.sleep;

public class MainPage {

    private final String nameInputField = "#name";
    private final String passwordInputField = "#password";
    private final String confirmationField = "#confirmation";
    private final String inputSearchField = "#mat-input-0";
    private final String orderButton = "#order";
    private final String ordersButton = "#orders";
    private final String startButton = "#start";
    private final String cancelButton = "#cancel";
    private final String terminateButton = "#terminate";
    private final String paymentButton = "#payment";
    private final String openOrderButton = "#open-order";
    private final String completeButton = "#complete";
    private final String okButton = "#ok";
    private final Integer standardDelay = 1000;
    private final Integer maxDelay = 5000;
    private final String starRatingButton = ".rate.star-rating > label:nth-of-type(%s)";
    private SelenideDriver browserDriver;


    public MainPage(SelenideDriver driver) {
        browserDriver = driver;
    }

    public MainPage openBrowser(String URL) {
        browserDriver.open(URL);
        return this;
    }

    public void closeBrowser() {
        browserDriver.close();
    }

    public MainPage login(String username, String password) {
        browserDriver.$(nameInputField).setValue(username);
        browserDriver.$(passwordInputField).setValue(password).pressEnter();
        browserDriver.$(confirmationField).waitUntil(visible, maxDelay);
        sleep(maxDelay);
        return this;
    }

    public MainPage passengerSearchDestination(String destination) {
        if(browserDriver.$(cancelButton).isDisplayed()){
            browserDriver.$(cancelButton).waitUntil(visible, maxDelay).click();
            sleep(standardDelay);

            browserDriver.$(okButton).waitUntil(visible, maxDelay).click();
            sleep(standardDelay);

            browserDriver.$(okButton).waitUntil(visible, maxDelay).click();
            sleep(standardDelay);

        }

        browserDriver.$(inputSearchField).waitUntil(visible, maxDelay).setValue(destination);
        sleep(standardDelay);
        Actions a = new Actions(browserDriver.getWebDriver());
        a.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
        sleep(standardDelay);
        return this;
    }

    public Double passengerOrderTaxi() {
        browserDriver.$(orderButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        browserDriver.$(By.xpath("//h1[contains(text(),'Are you ready to order a taxi?')]")).should(visible);
        Double rideFee = Double.parseDouble(browserDriver.$(By.xpath("//p[contains(text(),'Estimated price')]")).toString().split(" ")[2]);
        browserDriver.$(okButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        return rideFee;
    }

    public MainPage driverCheckOpenOrders() {
        browserDriver.$(ordersButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        return this;
    }

    public MainPage driverSelectFirstOpenOrder() {
        browserDriver.$(openOrderButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        return this;
    }

    public MainPage passengerSetOkOnYourRideHasBeenAcceptedBy(String cancellationFee) {
        browserDriver.$(By.xpath("//h1[contains(text(),'Your ride has been accepted by')]")).should(visible);
        String fee = browserDriver.$(By.xpath("//p[contains(text(),' there will be a penalty fee of')]")).toString().split(" ")[13];

        assertCancellationFees(fee, cancellationFee);
        browserDriver.$(okButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        return this;
    }

    public void assertCancellationFees(String fee1, String fee2){
        Double fee1Double = Double.parseDouble(fee1);
        Double fee2Double = Double.parseDouble(fee2);
        Assert.assertEquals(fee1Double,fee2Double,0.5);
    }

    public MainPage driverStartRide() {
        browserDriver.$(startButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        browserDriver.$(By.xpath("//h1[contains(text(),'Are you ready to start?')]")).should(visible);
        browserDriver.$(okButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        return this;
    }

    public MainPage driverCompleteRide() {
        browserDriver.$(completeButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        browserDriver.$(By.xpath("//h1[contains(text(),'Are you ready to complete the order?')]")).should(visible);
        browserDriver.$(okButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        return this;
    }

    public MainPage passengerCancelRide(String cancellationFee) {
        browserDriver.$(cancelButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        browserDriver.$(By.xpath("//h1[contains(text(),'Do you really want to cancel the order?')]")).should(visible);
        String rideFee = browserDriver.$(By.xpath("//p[contains(text(),'There will be a penalty fee of')]")).toString().split(" ")[7];
        assertCancellationFees(cancellationFee, rideFee);
        browserDriver.$(okButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        return this;
    }

    public MainPage driverTerminateRide() {
        browserDriver.$(terminateButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        browserDriver.$(By.xpath("//h1[contains(text(),'Do you really want to terminate the order?')]")).should(visible);
        browserDriver.$(okButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        return this;
    }


    public MainPage passengerSetOkOnYouHaveArrived() {
        browserDriver.$(By.xpath("//h1[contains(text(),'You have arrived!')]")).should(visible);
        browserDriver.$(okButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        return this;
    }


    public MainPage passengerPayForARide() {
        browserDriver.$(paymentButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        browserDriver.$(okButton).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        return this;
    }

    public MainPage passengerSetRideRating(String rating) {
        browserDriver.$(String.format(starRatingButton, rating)).waitUntil(visible, maxDelay).click();
        sleep(standardDelay);
        return this;
    }

    public MainPage acceptRideSummary(String status, String cost) {
        browserDriver.$(By.xpath(String.format("//p[contains(text(),'Status: %s')]", status))).should(visible);
        if(!status.equals("TERMINATED") && !status.equals("CANCELLED")) {
            browserDriver.$(By.xpath(String.format("//p[contains(text(),'Cost: %s')]", cost))).should(visible);
        }
        browserDriver.$(okButton).waitUntil(visible, maxDelay).click();
        return this;
    }

}
