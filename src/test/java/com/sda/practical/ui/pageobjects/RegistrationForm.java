package com.sda.practical.ui.pageobjects;

import com.codeborne.selenide.SelenideDriver;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;

import java.io.File;
import java.util.List;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class RegistrationForm {

    private final String registerButton = "#register";
    private final String profileButton = "#profile";
    private final String registerPassenger = "#option-passenger";
    private final String registerDriver = "#option-driver";
    private final String modifyProfileButton = "#modify-profile";
    private final String reportLostItemButton = "#report-lost-item";
    private final String lostItemField = "#lostitem";
    private final String lostItemMenu = "#lost-found-items";
    private final String historyButton = "#history";
    private final String resolveButton = "#resolve";
    private final String lostItemList = "#lost-item-list";
    private final String blockedUser = ".mat-list-item.blocked-user";
    private final String activateDeactivateButton = "#activate-deactivate";
    private final String activeUser = "#active-user";
    private final String lostOrderDropDown = "#lost-order-list";
    private final String addPictureButton = "#add-picture";
    private final String selectFileInput = "#select-file";
    private final String uploadFileButton = "#upload-file-button";
    private final String listOfRides = "#list-of-rides";
    private final String saveButton = "#save";
    private final String saveLostItem = "#save-lost-item";
    private final String profileUpdateConfirmation = "#profile-update-confirmation";
    private final String firstnameField = "#firstname";
    private final String lastnameField = "#lastname";
    private final String usernameField = "#username";
    private final String emailField = "#email";
    private final String phoneNumberField = "#phoneNumber";
    private final String countryField = "#country";
    private final String creditCardNumberField = "#creditCardNumber";
    private final String ccvField = "#ccv";
    private final String locationField = "#location";
    private final String carModelField = "#carModel";
    private final String licenseNumberField = "#licenseNumber";
    private final String carPlateNumberField = "#carPlateNumber";
    private final String carManufacturingYearField = "#carManufacturingYear";
    private final String birthdateField = "#birthdate";
    private final String expirationDateField = "#expirationDate";
    private final String amountOfSeatsField = "#amountOfSeats";
    private final String carColorField = "#carColor";
    private final String passwordField = "#password";
    private final String checkBoxPrivacy = "#check";
    private final String registerSubmitButton = "#register-button";
    private final String registrationConfirmationText = "#successful-registration";
    private final Integer standardDelay = 1000;
    private final Integer maxDelay = 5000;


    private SelenideDriver browserDriver;

    public RegistrationForm(SelenideDriver driver) {
        browserDriver = driver;
    }

    public RegistrationForm openBrowser(String URL) {
        browserDriver.open(URL);
        return this;
    }

    public RegistrationForm navigateToRegistrationForm() {
        browserDriver.$(registerButton).waitUntil(visible, maxDelay).click();
        return this;
    }


    public RegistrationForm navigateToProfilePage() {
        browserDriver.$(profileButton).waitUntil(visible, maxDelay).click();
        return this;
    }

    public RegistrationForm openModifyProfile(){
        browserDriver.$(modifyProfileButton).waitUntil(visible, maxDelay).click();
        return this;
    }

    public RegistrationForm openLostItem(){
        browserDriver.$(reportLostItemButton).waitUntil(visible, maxDelay).click();
        return this;
    }
    public RegistrationForm openHistory(){
        browserDriver.$(historyButton).waitUntil(visible, maxDelay).click();
        return this;
    }

    public RegistrationForm openHistoricalRide(){
        browserDriver.$(listOfRides).waitUntil(visible, maxDelay).click();
        browserDriver.$(By.xpath("//p[contains(text(),'Date')]")).waitUntil(visible, maxDelay);
        browserDriver.$(By.xpath("//p[contains(text(),'From')]")).waitUntil(visible, maxDelay);
        browserDriver.$(By.xpath("//p[contains(text(),'Where')]")).waitUntil(visible, maxDelay);
        return this;
    }

    public RegistrationForm submitLostItem(){
        browserDriver.$(lostItemField).waitUntil(visible, maxDelay).setValue(generateRandomText(10));
        browserDriver.$(lostOrderDropDown).waitUntil(visible, maxDelay).selectOption(0);
        browserDriver.$(saveLostItem).waitUntil(visible, maxDelay).click();
        return this;
    }

    public RegistrationForm addPicture(){
        browserDriver.$(addPictureButton).waitUntil(visible, maxDelay).click();
        File file = browserDriver.$(selectFileInput).uploadFile(new File("src/test/resources/prof.JPG"));
        browserDriver.$(uploadFileButton).waitUntil(visible, maxDelay).click();
        return this;
    }

    public String generateRandomText(Integer size) {
        int length = size;
        boolean useLetters = true;
        boolean useNumbers = false;
        return RandomStringUtils.random(length, useLetters, useNumbers);
    }

    public String generateRandomNumber(Integer size) {
        int length = size;
        boolean useLetters = false;
        boolean useNumbers = true;
        return RandomStringUtils.random(length, useLetters, useNumbers);
    }


    public RegistrationForm registerPassenger(String birthdate, String expirationDate, String password) {
        browserDriver.$(registerPassenger).waitUntil(visible, maxDelay).click();
        browserDriver.$(firstnameField).waitUntil(visible, maxDelay).setValue(generateRandomText(5));
        browserDriver.$(lastnameField).waitUntil(visible, maxDelay).setValue(generateRandomText(5));
        browserDriver.$(usernameField).waitUntil(visible, maxDelay).setValue(generateRandomText(5));
        browserDriver.$(emailField).waitUntil(visible, maxDelay).setValue(generateRandomText(5) + "@mail.com");
        browserDriver.$(birthdateField).waitUntil(visible, maxDelay).setValue(birthdate);
        browserDriver.$(expirationDateField).waitUntil(visible, maxDelay).setValue(expirationDate);
        browserDriver.$(phoneNumberField).waitUntil(visible, maxDelay).setValue(generateRandomNumber(8));
        browserDriver.$(countryField).waitUntil(visible, maxDelay).setValue(generateRandomText(6));
        browserDriver.$(creditCardNumberField).waitUntil(visible, maxDelay).setValue(generateRandomNumber(16));
        browserDriver.$(ccvField).waitUntil(visible, maxDelay).setValue(generateRandomNumber(3));
        browserDriver.$(locationField).waitUntil(visible, maxDelay).setValue(generateRandomText(8));
        browserDriver.$(passwordField).waitUntil(visible, maxDelay).setValue(password);
        browserDriver.$(checkBoxPrivacy).waitUntil(visible, maxDelay).click();
        browserDriver.$(registerSubmitButton).waitUntil(visible, maxDelay).click();
        browserDriver.$(registrationConfirmationText).waitUntil(visible, maxDelay);
        return this;
    }

    public RegistrationForm updatePassengerProfile(String expirationDate, String password) {
        if (browserDriver.$(expirationDateField).waitUntil(visible, maxDelay).getValue().equals("")) {
            browserDriver.$(expirationDateField).waitUntil(visible, maxDelay).setValue(expirationDate);
        }
        browserDriver.$(countryField).waitUntil(visible, maxDelay).setValue(generateRandomText(8));
        browserDriver.$(locationField).waitUntil(visible, maxDelay).setValue(generateRandomText(8));
        browserDriver.$(passwordField).waitUntil(visible, maxDelay).setValue(password);
        browserDriver.$(saveButton).waitUntil(visible, maxDelay).click();
        browserDriver.$(profileUpdateConfirmation).waitUntil(visible, maxDelay);
        return this;
    }

    public RegistrationForm updateAdminProfile( String password) {
        browserDriver.$(emailField).waitUntil(visible, maxDelay).setValue(generateRandomText(5)+"@"+generateRandomText(4)+".com");
        browserDriver.$(phoneNumberField).waitUntil(visible, maxDelay).setValue(generateRandomNumber(8));
        browserDriver.$(passwordField).waitUntil(visible, maxDelay).setValue(password);
        browserDriver.$(saveButton).waitUntil(visible, maxDelay).click();
        browserDriver.$(profileUpdateConfirmation).waitUntil(visible, maxDelay);
        return this;
    }

    public RegistrationForm activateDeactivateUser(){
        browserDriver.$(activateDeactivateButton).waitUntil(visible, maxDelay).click();
        browserDriver.$(activeUser).waitUntil(visible, maxDelay).click();
        browserDriver.$(blockedUser).shouldBe(visible);
        browserDriver.$(activeUser).waitUntil(visible, maxDelay).click();
        return this;
    }

    public RegistrationForm resolveLostItemCase(){
        browserDriver.$(lostItemMenu).waitUntil(visible, maxDelay).click();
//        Integer list = browserDriver.$$(lostItemList).filterBy(visible).size();
        browserDriver.$(lostItemList).waitUntil(visible, maxDelay).click();
        browserDriver.$(resolveButton).waitUntil(visible, maxDelay).click();
//        Assert.assertEquals((int) list, list - 1);
        return this;
    }

    public RegistrationForm registerDriver(String carManufacturingYear, String password) {
        browserDriver.$(registerDriver).waitUntil(visible, maxDelay).click();
        browserDriver.$(firstnameField).waitUntil(visible, maxDelay).setValue(generateRandomText(10));
        browserDriver.$(lastnameField).waitUntil(visible, maxDelay).setValue(generateRandomText(10));
        browserDriver.$(usernameField).waitUntil(visible, maxDelay).setValue(generateRandomText(10));
        browserDriver.$(emailField).waitUntil(visible, maxDelay).setValue(generateRandomText(5) + "@mail.com");
        browserDriver.$(phoneNumberField).waitUntil(visible, maxDelay).setValue(generateRandomNumber(8));
        browserDriver.$(carManufacturingYearField).waitUntil(visible, maxDelay).setValue(carManufacturingYear);
        browserDriver.$(carModelField).waitUntil(visible, maxDelay).setValue(generateRandomText(6));
        browserDriver.$(licenseNumberField).waitUntil(visible, maxDelay).setValue(generateRandomText(5));
        browserDriver.$(carPlateNumberField).waitUntil(visible, maxDelay).setValue(generateRandomNumber(3)+generateRandomText(3));
        browserDriver.$(amountOfSeatsField).waitUntil(visible, maxDelay).setValue(generateRandomNumber(1));
        browserDriver.$(carColorField).waitUntil(visible, maxDelay).setValue(generateRandomText(6));
        browserDriver.$(passwordField).waitUntil(visible, maxDelay).setValue(password);
        browserDriver.$(checkBoxPrivacy).waitUntil(visible, maxDelay).click();
        browserDriver.$(registerSubmitButton).waitUntil(visible, maxDelay).click();
        browserDriver.$(registrationConfirmationText).waitUntil(visible, maxDelay);
        return this;
    }

    public void closeBrowser() {
        browserDriver.close();
    }

    public RegistrationForm updateDriverProfile(String manufactoringYear, String password) {
        if (browserDriver.$(carManufacturingYearField).waitUntil(visible, maxDelay).getValue().equals("")) {
            browserDriver.$(carManufacturingYearField).waitUntil(visible, maxDelay).setValue(manufactoringYear);
        }
        browserDriver.$(carColorField).waitUntil(visible, maxDelay).setValue(generateRandomText(6));
        browserDriver.$(carModelField).waitUntil(visible, maxDelay).setValue(generateRandomText(6));
        browserDriver.$(passwordField).waitUntil(visible, maxDelay).setValue(password);
        browserDriver.$(saveButton).waitUntil(visible, maxDelay).click();
        browserDriver.$(profileUpdateConfirmation).waitUntil(visible, maxDelay);
        return this;
    }
}
