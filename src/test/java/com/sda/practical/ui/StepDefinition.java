package com.sda.practical.ui;

import com.codeborne.selenide.*;
import com.sda.practical.ui.pageobjects.RegistrationForm;
import com.sda.practical.ui.pageobjects.MainPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.text.DecimalFormat;

public class StepDefinition {

    private final String URL = "http://localhost:4200";
    private static DecimalFormat df2 = new DecimalFormat("#.##");
    private MainPage passenger;
    private MainPage driver;
    private MainPage admin;
    private RegistrationForm passengerRegForm;
    private RegistrationForm driverRegForm;
    private RegistrationForm adminRegForm;
    private final String driverUsername = "driver";
    private final String passengerUsername = "passenger";
    private final String adminUsername = "admin";
    private final String password = "123456";
    private final String destination = "Klara";

    @Before
    public void setup() {
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver7203.exe");
        SelenideDriver passengerDriver = new SelenideDriver(new SelenideConfig());
        SelenideDriver driversDriver = new SelenideDriver(new SelenideConfig());
        SelenideDriver adminsDriver = new SelenideDriver(new SelenideConfig());
        passenger = new MainPage(passengerDriver);
        driver = new MainPage(driversDriver);
        admin = new MainPage(adminsDriver);
        passengerRegForm = new RegistrationForm(passengerDriver);
        driverRegForm = new RegistrationForm(driversDriver);
        adminRegForm = new RegistrationForm(adminsDriver);
    }

    @After
    public void tearDown() {
        passenger.closeBrowser();
        driver.closeBrowser();
    }

    @Test
    public void passengerDriverE2eWorkflow() {
        Double rideFee = passenger.openBrowser(URL)
                .login(passengerUsername, password)
                .passengerSearchDestination(destination)
                .passengerOrderTaxi();

        driver.openBrowser(URL)
                .login(driverUsername,password)
                .driverCheckOpenOrders()
                .driverSelectFirstOpenOrder();

        passenger.passengerSetOkOnYourRideHasBeenAcceptedBy(df2.format(rideFee/2));

        driver.driverStartRide()
                .driverCompleteRide();

        passenger.passengerSetOkOnYouHaveArrived()
                .passengerPayForARide()
                .passengerSetRideRating("1");

        driver.acceptRideSummary("PAID", df2.format(rideFee));

        passenger.closeBrowser();

        driver.closeBrowser();
    }

    @Test
    public void addLostItem(){
        passengerDriverE2eWorkflow();
        passenger.openBrowser(URL).login(passengerUsername,password);
        passengerRegForm
                .navigateToProfilePage()
                .openLostItem()
                .submitLostItem();
        passengerRegForm.closeBrowser();
    }

    @Test
    public void addFoundItem(){
        passengerDriverE2eWorkflow();
        driver.openBrowser(URL).login(driverUsername,password);
        driverRegForm
                .navigateToProfilePage()
                .openLostItem()
                .submitLostItem();
        driverRegForm.closeBrowser();
    }

    @Test
    public void addUserPicture(){
        driver.openBrowser(URL).login(driverUsername,password);
        driverRegForm
                .navigateToProfilePage()
                .addPicture();
        driverRegForm.closeBrowser();
    }

    @Test
    public void checkHistoricalRide(){
        passengerDriverE2eWorkflow();
        driver.openBrowser(URL).login(driverUsername,password);
        driverRegForm
                .navigateToProfilePage()
                .openHistory()
                .openHistoricalRide();
        driverRegForm.closeBrowser();
    }

    @Test
    public void passengerOrderAndCancel() {
        passenger.openBrowser(URL)
                .login(passengerUsername, password)
                .passengerSearchDestination(destination)
                .passengerOrderTaxi();

        passenger.passengerCancelRide("0.00")
                .acceptRideSummary("CANCELLED", "0.00")
                .closeBrowser();
    }

    @Test
    public void passengerCancelAcceptedRide() {
        Double rideFee = passenger.openBrowser(URL)
                .login(passengerUsername, password)
                .passengerSearchDestination(destination)
                .passengerOrderTaxi();
        String rideFeeString = df2.format(rideFee/2);

        driver.openBrowser(URL)
                .login(driverUsername, password)
                .driverCheckOpenOrders()
                .driverSelectFirstOpenOrder();
        passenger.passengerSetOkOnYourRideHasBeenAcceptedBy(rideFeeString);

        passenger.passengerCancelRide(rideFeeString)
                .acceptRideSummary("CANCELLED",rideFeeString)
                .closeBrowser();
    }

    @Test
    public void driverTerminateRide() {
        Double rideFee = passenger.openBrowser(URL)
                .login(passengerUsername, password)
                .passengerSearchDestination(destination)
                .passengerOrderTaxi();

        driver.openBrowser(URL)
                .login(driverUsername, password)
                .driverCheckOpenOrders()
                .driverSelectFirstOpenOrder();
        passenger.passengerSetOkOnYourRideHasBeenAcceptedBy(df2.format(rideFee/2));

        driver.driverTerminateRide().closeBrowser();

        passenger.acceptRideSummary("TERMINATED", "0.00")
                .closeBrowser();
    }

    @Test
    public void driverTerminatAfterStartRide() {
        Double rideFee = passenger.openBrowser(URL)
                .login(passengerUsername, password)
                .passengerSearchDestination(destination)
                .passengerOrderTaxi();

        driver.openBrowser(URL)
                .login(driverUsername, password)
                .driverCheckOpenOrders()
                .driverSelectFirstOpenOrder();
        passenger.passengerSetOkOnYourRideHasBeenAcceptedBy(df2.format(rideFee/2));

        driver.driverStartRide()
                .driverTerminateRide()
                .closeBrowser();

        passenger.acceptRideSummary("TERMINATED", "0.00")
                .closeBrowser();
    }

    @Test
    public void driverTerminatAfterCompleteRide() {
        Double rideFee = passenger.openBrowser(URL)
                .login(passengerUsername, password)
                .passengerSearchDestination(destination)
                .passengerOrderTaxi();

        driver.openBrowser(URL)
                .login(driverUsername, password)
                .driverCheckOpenOrders()
                .driverSelectFirstOpenOrder();
        passenger.passengerSetOkOnYourRideHasBeenAcceptedBy(df2.format(rideFee/2));

        driver.driverStartRide()
                .driverCompleteRide();

        passenger.passengerSetOkOnYouHaveArrived();

        driver.driverTerminateRide()
                .closeBrowser();

        passenger.acceptRideSummary("TERMINATED", "0.00")
                .closeBrowser();
    }

    @Test
    public void registerPassenger(){
        passengerRegForm.openBrowser(URL)
                .navigateToRegistrationForm()
                .registerPassenger(
                        "10/3/2001",
                        "10/3/2025",
                        password);
        passengerRegForm.closeBrowser();
    }

    @Test
    public void updatePassenger(){
        passenger.openBrowser(URL).login(passengerUsername,password);
        passengerRegForm
                .navigateToProfilePage()
                .updatePassengerProfile("1/1/2030",password);
        passengerRegForm.closeBrowser();
    }

    @Test
    public void updateDriver(){
        driver.openBrowser(URL).login(driverUsername,password);
        driverRegForm
                .navigateToProfilePage()
                .updateDriverProfile("1/1/2030",password);
        driverRegForm.closeBrowser();
    }

    @Test
    public void updateAdmin(){
        admin.openBrowser(URL).login(adminUsername,password);
        adminRegForm
                .updateAdminProfile(password);
        adminRegForm.closeBrowser();
    }

    @Test
    public void deactivateUser(){
        admin.openBrowser(URL).login(adminUsername,password);
        adminRegForm
                .activateDeactivateUser();
        adminRegForm.closeBrowser();
    }

    @Test
    public void adminResolveLostItemCase(){
        addFoundItem();
        admin.openBrowser(URL).login(adminUsername,password);
        adminRegForm.resolveLostItemCase();
        adminRegForm.closeBrowser();
    }

    @Test
    public void registerDriver(){
        passengerRegForm.openBrowser(URL)
                .navigateToRegistrationForm()
                .registerDriver(
                        "1/1/2019",
                        password);
        passengerRegForm.closeBrowser();
    }
}
