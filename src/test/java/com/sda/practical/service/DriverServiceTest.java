package com.sda.practical.service;

import com.sda.practical.exception.GeneralException;
import com.sda.practical.model.Driver;
import com.sda.practical.service.impl.DriverServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@Sql(scripts = "/test-dataset.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class DriverServiceTest {

    @Autowired
    private DriverServiceImpl driverService;

    @Test
    public void getOneSuccess() {
        Driver driver = driverService.getOne(22L);
        Assert.assertNull(driver);
    }

    @Test
    public void getOneFail() {
        Driver driver = driverService.getOne(0L);
        Assert.assertNull(driver);
    }

    @Test
    public void saveDriverSuccess() {
        Driver driver = new Driver();
        driver.setLicenseNumber("test");
        driver.setId(111L);
        driver.setAmountOfSeats(4);
        driver.setCarColor("blue");
        driver.setCarManufacturingYear("2015");
        driver.setCarPlateNumber("123aAS");
        driver.setCarModel("VW");
        Driver saveDriver = driverService.saveDriver(driver);
        Assert.assertNotNull(saveDriver);
    }

    @Test
    public void updateDriverSuccess() {
        Driver driver = driverService.getOne(1L);
        driver.setAmountOfSeats(2);
        driver.setCarColor("TEST");
        driver.setCarManufacturingYear("TEST");
        driver.setCarPlateNumber("TEST");
        driver.setCarModel("TEST");
        driverService.updateDriver(driver);
        Driver driverUpdated = driverService.getOne(1L);
        Assert.assertEquals(driverUpdated.getAmountOfSeats(), driver.getAmountOfSeats());
        Assert.assertEquals(driverUpdated.getCarColor(), driver.getCarColor());
        Assert.assertEquals(driverUpdated.getCarManufacturingYear(), driver.getCarManufacturingYear());
        Assert.assertEquals(driverUpdated.getCarPlateNumber(), driver.getCarPlateNumber());
        Assert.assertEquals(driverUpdated.getCarModel(), driver.getCarModel());
    }

    @Test (expected = GeneralException.class)
    public void updateDriverFail() {
        Driver driver = driverService.getOne(1L);
        driver.setAmountOfSeats(3);
        driver.setId(10L);
        driverService.updateDriver(driver);
    }

    @Test(expected = GeneralException.class)
    public void saveDriverFail() {
        Driver driver = new Driver();
        driver.setLicenseNumber("test");
        driver.setId(111L);
        driver.setAmountOfSeats(4);
        driver.setCarColor("blue");
        driver.setCarManufacturingYear("2015");
        driver.setCarPlateNumber("123aAS");
        driver.setCarModel("VW");
        driverService.saveDriver(driver);
        driverService.saveDriver(driver);
    }
}
