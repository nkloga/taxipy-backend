package com.sda.practical.service;

import com.sda.practical.dto.LostItemDto;
import com.sda.practical.model.utils.LostItemStatus;
import com.sda.practical.model.utils.LostItemType;
import com.sda.practical.service.utlis.SecurityContextUpdate;
import com.sda.practical.service.utlis.TestRoles;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.Resources;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@Sql(scripts = "/test-dataset.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class LostItemServiceTest {

    @Autowired
    private LostItemService lostItemService;

    private LostItemDto generateLostItem(Long orderId, String description) {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER_ONE_ORDER_LOSTITEM);
        LostItemDto lostItemDto = new LostItemDto();
        lostItemDto.setOrder(orderId);
        lostItemDto.setDescription(description);
        lostItemDto.setType(LostItemType.LOST);
        return lostItemDto;
    }

    @Test
    public void getListOfLostItems() {
        Long orderId = 26L;
        String description = "Some description text";
        LostItemDto lostItemDto = generateLostItem(orderId, description);
        LostItemDto lostItemDto2 = generateLostItem(orderId, description);
        LostItemDto lostItemDto3 = generateLostItem(orderId, description);
        lostItemService.submitLostItem(lostItemDto);
        lostItemService.submitLostItem(lostItemDto2);
        lostItemService.submitLostItem(lostItemDto3);
        Resources<LostItemDto> list = lostItemService.getListOfLostItems();
        Assert.assertEquals(list.getContent().size(), 4);
    }

    @Test
    public void submitLostItem() {
        Long orderId = 26L;
        String description = "Some description text";
        LostItemDto lostItemDto = generateLostItem(orderId, description);
        LostItemDto lostItem = lostItemService.submitLostItem(lostItemDto);
        Assert.assertEquals(lostItem.getOrder(), orderId);
        Assert.assertEquals(lostItem.getDescription(), description);
        Assert.assertEquals(lostItem.getType(), LostItemType.LOST);
        Assert.assertEquals(lostItem.getStatus(), LostItemStatus.SUBMITTED);
    }

    @Test
    public void getOne() {
        LostItemDto lostItem = lostItemService.getOne(27L);
        Assert.assertEquals(lostItem.getOrder(), 26L, 0.1);
        Assert.assertEquals(lostItem.getStatus(), LostItemStatus.SUBMITTED);
        Assert.assertEquals(lostItem.getType(), LostItemType.LOST);
        Assert.assertEquals(lostItem.getDescription(), "description");
    }


    @Test
    public void close() {
        LostItemDto closedItem = lostItemService.close(27L);
        Assert.assertEquals(closedItem.getStatus(), LostItemStatus.CLOSED);
    }
}
