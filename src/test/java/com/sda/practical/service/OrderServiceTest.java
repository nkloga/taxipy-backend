package com.sda.practical.service;

import com.sda.practical.dto.*;
import com.sda.practical.exception.GeneralException;
import com.sda.practical.model.utils.OrderStatus;
import com.sda.practical.service.utlis.SecurityContextUpdate;
import com.sda.practical.service.utlis.TestRoles;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@Sql(scripts = "/test-dataset.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class OrderServiceTest {

    @Autowired
    private OrderService orderService;

    @Test
    public void getAllDriversHistoryOrdersSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER);
        Resources<Resource<OrderDto>> orderlist = orderService.getAllHistoryOrders();
        Assert.assertTrue(orderlist.getContent().size() > 0);
    }

    @Test(expected = NullPointerException.class)
    public void getAllDriversHistoryOrdersFail() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER_WRONG_ID);
        orderService.getAllHistoryOrders();
    }

    @Test
    public void getAllPassengersHistoryOrdersSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        Resources<Resource<OrderDto>> orderlist = orderService.getAllHistoryOrders();
        Assert.assertTrue(orderlist.getContent().size() > 0);
    }

    @Test
    public void getDriversLastOpenOrderSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER_WITH_ONE_ORDER);
        Resource<OrderDto> order = orderService.getLastOpenOrder();
        Assert.assertNotNull(order.getContent());
    }

    @Test
    public void getPassengerLastOpenOrderSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER_WITH_ONE_ORDER);
        Resource<OrderDto> order = orderService.getLastOpenOrder();
        Assert.assertNotNull(order.getContent());
    }

    @Test
    public void getDriverOpenOrdersSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER);
        Resources<Resource<OrderDto>> orderlist = orderService.getAllOpenOrders();
        Assert.assertTrue(orderlist.getContent().size() > 0);
    }

    @Test
    public void getPassengerOpenOrdersSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        Resources<Resource<OrderDto>> orderlist = orderService.getAllOpenOrders();
        Assert.assertTrue(orderlist.getContent().size() > 0);
    }

    @Test
    public void getLastPassengersLastOrderNullSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER_NO_ORDER);
        Resource<OrderDto> order = orderService.getLastOpenOrder();
        Assert.assertNull(order);
    }

    @Test
    public void setOrderFeedbackSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        Resource<OrderDto> order = orderService.feedback(12L, 3);
        Assert.assertEquals(3, (int) order.getContent().getRating());
    }

    @Test(expected = GeneralException.class)
    public void setOrderFeedbackIncorrectIdFail() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        Resource<OrderDto> order = orderService.feedback(-1L, 3);
        Assert.assertEquals(3, (int) order.getContent().getRating());
    }

    @Test(expected = GeneralException.class)
    public void setOrderFeedbackIncorrectRatingFail() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        Resource<OrderDto> order = orderService.feedback(12L, 8);
        Assert.assertEquals(3, (int) order.getContent().getRating());
    }

    @Test
    public void getOneTestSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        Resource<OrderDto> order = orderService.getOrderDtoById(11L);
        Assert.assertNotNull(order.getContent());
    }

    @Test(expected = GeneralException.class)
    public void getOneTestFail() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        orderService.getOrderDtoById(-1L);
    }

    @Test
    public void addOrderTestSuccess() {
        OrderDto orderDto = createSampleOrder();
        UserDto userDto = SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        orderDto.setPassenger(userDto);
        OrderDto addedOrder = orderService.add(orderDto).getContent();
        Assert.assertNotNull(addedOrder.getOrderId());
    }

    private OrderDto createSampleOrder() {
        OrderDto orderDto = new OrderDto();
        orderDto.setStatus(OrderStatus.IN_PROGRESS);
        orderDto.setFromLat("s");
        orderDto.setWhereLat("e");
        orderDto.setFromLong("s");
        orderDto.setWhereLong("e");
        orderDto.setDistance(null);
        orderDto.setTime(null);
        return orderDto;
    }

    @Test(expected = GeneralException.class)
    public void addTestMissingCoordinatesFail() {
        OrderDto orderDto = createSampleOrder();
        UserDto userDto = SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        orderDto.setFromLat(null);
        orderDto.setWhereLat(null);
        orderDto.setFromLong(null);
        orderDto.setWhereLong(null);
        orderDto.setPassenger(userDto);
        orderService.add(orderDto);
    }

    @Test
    public void completeTestSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        Resource<OrderDto> order = orderService.complete(17L);
        Assert.assertEquals(order.getContent().getStatus().toString(), OrderStatus.COMPLETED.toString());
    }

    @Test(expected = GeneralException.class)
    public void completeTestFail() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        orderService.complete(16L);
    }

    @Test
    public void startTestSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        Resource<OrderDto> order = orderService.start(22L);
        Assert.assertEquals(order.getContent().getStatus().toString(), OrderStatus.STARTED.toString());
    }

    @Test(expected = GeneralException.class)
    public void startTestFail() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        orderService.start(21L);
    }

    @Test
    public void cancelByPassengerTestSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        Resource<OrderDto> order = orderService.cancel(16L);
        Assert.assertEquals(order.getContent().getStatus().toString(), OrderStatus.CANCELLED.toString());
    }

    @Test(expected = GeneralException.class)
    public void cancelByDriverTestFail() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER);
        orderService.cancel(16L);
    }

    @Test
    public void cancelSetZeroCostTestSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        OrderDto cancelledOrder = orderService.cancel(15L).getContent();
        Assert.assertEquals(cancelledOrder.getCost(), 0.0, 0.1);
    }

    @Test
    public void cancelSetHalfCostTestSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        OrderDto cancelledOrder = orderService.cancel(14L).getContent();
        Assert.assertEquals(cancelledOrder.getCost(), 8.0, 0.1);
    }

    @Test
    public void acceptTestSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        Resource<OrderDto> order = orderService.accept(19L);
        Assert.assertEquals(order.getContent().getStatus().toString(), OrderStatus.ACCEPTED.toString());
    }

    @Test(expected = GeneralException.class)
    public void acceptTestFail() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        orderService.accept(17L);
    }

    @Test
    public void terminateTestSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        Resource<OrderDto> order = orderService.terminate(18L);
        Assert.assertEquals(order.getContent().getStatus().toString(), OrderStatus.TERMINATED.toString());
    }

    @Test(expected = GeneralException.class)
    public void terminateTestFail() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        orderService.terminate(16L);
    }

    @Test
    public void payTestSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        Resource<OrderDto> order = orderService.pay(20L);
        Assert.assertEquals(order.getContent().getStatus().toString(), OrderStatus.PAID.toString());
    }

    @Test(expected = GeneralException.class)
    public void payTestFail() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        orderService.pay(16L);
    }
}
