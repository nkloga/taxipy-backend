package com.sda.practical.service;

import com.sda.practical.exception.GeneralException;
import com.sda.practical.model.Passenger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@Sql(scripts = "/test-dataset.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class PassengerServiceTest {

    @Autowired
    private PassengerService passengerService;

    @Test
    public void getOneSuccess() {
        Passenger passenger = passengerService.getOne(22L);
        Assert.assertNull(passenger);
    }

    @Test
    public void getOneFail() {
        Passenger passenger = passengerService.getOne(0L);
        Assert.assertNull(passenger);
    }

    @Test
    public void savePassengerSuccess() {
        Passenger passenger = new Passenger();
        passenger.setExpirationDate("date");
        passenger.setCcv(123);
        passenger.setCreditCardNumber("1234123412341111");
        passenger.setBirthdate("date");
        passenger.setCountry("Est");
        passenger.setLocation("Tall");
        Passenger savePassenger = passengerService.savePassenger(passenger);
        Assert.assertNotNull(savePassenger);
    }

    @Test(expected = GeneralException.class)
    public void savePassengerFail() {
        Passenger passenger = new Passenger();
        passenger.setCreditCardNumber("CRED123412341234");
        passengerService.savePassenger(passenger);
    }

    @Test
    public void updatePassengerSuccess() {
        Passenger passenger = passengerService.getOne(5L);
        passenger.setExpirationDate("date");
        passenger.setCcv(123);
        passenger.setCreditCardNumber("123");
        passenger.setCountry("Est");
        passenger.setLocation("Tall");
        passengerService.updatePassenger(passenger);
        Passenger passengerUpdated = passengerService.getOne(5L);
        Assert.assertEquals(passengerUpdated.getExpirationDate(), passenger.getExpirationDate());
        Assert.assertEquals(passengerUpdated.getCcv(), passenger.getCcv());
        Assert.assertEquals(passengerUpdated.getCreditCardNumber(), passenger.getCreditCardNumber());
        Assert.assertEquals(passengerUpdated.getBirthdate(), passenger.getBirthdate());
        Assert.assertEquals(passengerUpdated.getCountry(), passenger.getCountry());
    }

    @Test (expected = GeneralException.class)
    public void updatePassengerFail() {
        Passenger passenger = passengerService.getOne(5L);
        passenger.setId(1L);
        passengerService.updatePassenger(passenger);
    }
}
