package com.sda.practical.service.utlis;

import com.sda.practical.dto.UserDto;
import com.sda.practical.model.Authority;
import com.sda.practical.model.utils.AuthorityName;
import com.sda.practical.security.JwtUserFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class SecurityContextUpdate {

    public static UserDto setCurrentlyAuthenticatedUser(TestRoles role) {
        Long id = null;
        String username = "";
        AuthorityName authorityName = null;
        UserDto userDto;

        if (role == TestRoles.PASSENGER) {
            authorityName = AuthorityName.ROLE_PASSENGER;
            id = 6L;
            username = "test-user3";
        } else if (role == TestRoles.PASSENGER_WITH_ONE_ORDER) {
            authorityName = AuthorityName.ROLE_PASSENGER;
            id = 8L;
            username = "test-user4";
        } else if (role == TestRoles.ADMIN) {
            authorityName = AuthorityName.ROLE_ADMIN;
            id = 34L;
            username = "admin";
        } else if (role == TestRoles.DRIVER) {
            authorityName = AuthorityName.ROLE_DRIVER;
            id = 2L;
            username = "test-user1";
        } else if (role == TestRoles.DRIVER_WITH_ONE_ORDER) {
            authorityName = AuthorityName.ROLE_DRIVER;
            id = 4L;
            username = "test-user2";
        } else if (role == TestRoles.PASSENGER_NO_ORDER) {
            authorityName = AuthorityName.ROLE_PASSENGER;
            id = 10L;
            username = "test-user5";
        } else if (role == TestRoles.PASSENGER_ONE_ORDER_LOSTITEM) {
            authorityName = AuthorityName.ROLE_PASSENGER;
            id = 38L;
            username = "test-user6";
        } else if (role == TestRoles.DRIVER_WRONG_ID) {
            authorityName = AuthorityName.ROLE_DRIVER;
            id = 12L;
            username = "test";
        }

        List<Authority> list = new ArrayList<>();
        list.add(new Authority(authorityName));
        List<GrantedAuthority> passAuthList = JwtUserFactory.mapToGrantedAuthorities(list);
        userDto = new UserDto(id, username, "Admin", "Time", "email", "pw", passAuthList, true, new Date(), null, null, "ph");
        Authentication auth = new UsernamePasswordAuthenticationToken(userDto, null, passAuthList);
        SecurityContextHolder.getContext().setAuthentication(auth);
        return userDto;
    }
}
