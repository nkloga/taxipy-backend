package com.sda.practical.service;

import com.sda.practical.exception.GeneralException;
import com.sda.practical.model.DBFile;
import com.sda.practical.service.impl.DBFileStorageService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)

public class DBFileStorageServiceTest {

    @Autowired
    private DBFileStorageService dbFileStorageService;

    private MockMultipartFile getMockFile(String name, String type, String content) {
        return new MockMultipartFile("name", name, type, content.getBytes());
    }

    @Test
    public void storeFileTestSuccess() {
        String content = "test content, byte array";
        DBFile dbFile = dbFileStorageService.storeFile(getMockFile("filename.txt", "text/plain", content));
        String savedContent = new String(dbFile.getData());
        Assert.assertEquals(content, savedContent);
    }

    @Test(expected = GeneralException.class)
    public void storeFileIncorrectCharsTestFail() {
        String content = "test content, byte array 3";
        dbFileStorageService.storeFile(getMockFile("filename..txt", "text/plain", content));
    }

    @Test
    public void getFileTestSuccess() {
        String content = "another test content, byte array";
        DBFile dbFile = dbFileStorageService.storeFile(getMockFile("filename.txt", "text/plain", content));
        DBFile fileFromDB = dbFileStorageService.getFile(dbFile.getId());
        String savedContent = new String(fileFromDB.getData());
        Assert.assertEquals(savedContent, content);
    }

    @Test(expected = GeneralException.class)
    public void getFileTestFail() {
        dbFileStorageService.getFile("1");
    }
}