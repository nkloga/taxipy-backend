package com.sda.practical.service;

import com.sda.practical.dto.*;
import com.sda.practical.exception.GeneralException;
import com.sda.practical.model.Authority;
import com.sda.practical.model.Driver;
import com.sda.practical.model.Passenger;
import com.sda.practical.model.User;
import com.sda.practical.model.utils.AuthorityName;
import com.sda.practical.service.utlis.SecurityContextUpdate;
import com.sda.practical.service.utlis.TestRoles;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
@Sql(scripts = "/test-dataset.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void getListOfUsersTestSuccess() {
        List<AuthUserDto> list = userService.getListOfUsers();
        Assert.assertTrue(list.size() > 0);
    }

    @Test
    public void findByUserNameTestSuccess() {
        User user = userService.findByUserName("test-user5");
        Assert.assertNotNull(user);
    }

    @Test
    public void findByUserNameTestFail() {
        User user = userService.findByUserName("dummy-test");
        Assert.assertNull(user);
    }

    @Test
    public void registerDriverTestSuccess() {
        Authority authority = new Authority();
        authority.setAuthority(AuthorityName.ROLE_DRIVER);
        List<Authority> driverList = new ArrayList<>();
        driverList.add(authority);
        Driver driver = new Driver();
        driver.setCarModel("model");
        driver.setCarPlateNumber("123ASD");
        driver.setCarManufacturingYear("2314");
        driver.setCarColor("red");
        driver.setAmountOfSeats(5);
        driver.setLicenseNumber("4444444");
        driver.setDriversPhoto(null);
        String username = "test-driver";
        UserDto user = userService.register(new User(null, username, "Driver", "Maxx", "driver1@gmail.com", "123456", driverList, true, new Date(), driver, null, "54432341"));
        Assert.assertEquals(user.getUsername(), username);
    }

    @Test
    public void registerPassengerTestSuccess() {
        Authority authority = new Authority();
        authority.setAuthority(AuthorityName.ROLE_PASSENGER);
        List<Authority> passengerList = new ArrayList<>();
        passengerList.add(authority);
        Passenger passenger = new Passenger();
        passenger.setCreditCardNumber("CRED123411111111");
        passenger.setLocation("Loc");
        passenger.setCountry("Est");
        passenger.setBirthdate("123321");
        passenger.setCcv(123);
        passenger.setExpirationDate("123");
        String username = "test-driver";
        UserDto user = userService.register(new User(null, username, "Driver", "Maxx", "driver1@gmail.com", "123456", passengerList, true, new Date(), null, passenger, "54432341"));
        Assert.assertEquals(user.getUsername(), username);
    }

    @Test(expected = GeneralException.class)
    public void registerExistingEmailTestFail() {
        Authority authority = new Authority();
        authority.setAuthority(AuthorityName.ROLE_DRIVER);
        List<Authority> driverList = new ArrayList<>();
        driverList.add(authority);
        Driver driver = new Driver();
        driver.setCarModel("model");
        driver.setCarPlateNumber("123ASD");
        driver.setCarManufacturingYear("2314");
        driver.setCarColor("red");
        driver.setAmountOfSeats(5);
        driver.setLicenseNumber("4444444");
        driver.setDriversPhoto(null);
        String username = "test-driver";
        userService.register(new User(null, username, "Driver", "Maxxasd", "test@test1", "123456", driverList, true, new Date(), driver, null, "54432341"));
    }

    @Test(expected = GeneralException.class)
    public void registerExistingUsernameTestFail() {
        Authority authority = new Authority();
        authority.setAuthority(AuthorityName.ROLE_DRIVER);
        List<Authority> driverList = new ArrayList<>();
        driverList.add(authority);
        Driver driver = new Driver();
        driver.setCarModel("model");
        driver.setCarPlateNumber("123ASD");
        driver.setCarManufacturingYear("2314");
        driver.setCarColor("red");
        driver.setAmountOfSeats(5);
        driver.setLicenseNumber("4444444");
        driver.setDriversPhoto(null);
        String username = "test-user1";
        userService.register(new User(null, username, "Driver", "Maxxasda", "test@test1", "123456", driverList, true, new Date(), driver, null, "54432341"));
    }

    @Test
    public void authenticateTestSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        AuthUserDto user = userService.authenticate();
        Assert.assertEquals(user.getUsername(), "test-user3");
    }

    @Test
    public void updateCurrentUserTestSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        User user = userService.getCurrentUser();
        user.setEmail("updated");
        user.setPhoneNumber("user");
        user.setPassword("123456");
        userService.updateCurrentUser(user);
        User updatedUser = userService.getCurrentUser();
        Assert.assertEquals(updatedUser.getFirstname(),user.getFirstname());
        Assert.assertEquals(updatedUser.getLastname(),user.getLastname());
    }

    @Test (expected = GeneralException.class)
    public void updateCurrentUserTestFail() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        User user = userService.getCurrentUser();
        user.setEmail("updated");
        user.setPassword("12356");
        userService.updateCurrentUser(user);
    }

    @Test
    public void getCurrentUserTestSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.PASSENGER);
        User user = userService.getCurrentUser();
        Assert.assertSame(user.getAuthorities().get(0).getAuthority(), AuthorityName.ROLE_PASSENGER);
    }

    @Test
    public void blockUserTestSuccess() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER);
        AuthUserDto user = userService.blockUser(2L, false);
        Assert.assertFalse(user.isEnabled());
    }

    @Test
    public void blockUserTestFail() {
        SecurityContextUpdate.setCurrentlyAuthenticatedUser(TestRoles.DRIVER);
        AuthUserDto user = userService.blockUser(0L, false);
        Assert.assertNull(user);
    }
}
