package com.sda.practical.service;

import com.sda.practical.dto.AuthorityDto;
import com.sda.practical.model.utils.AuthorityName;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@Sql(scripts = "/test-dataset.sql")  // after each test case execution, this sql will be run
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class AuthorityServiceTest {

    @Autowired
    AuthorityService authorityService;

    @Test
    public void getAuthoritySuccess() {
        AuthorityDto authority = authorityService.getAuthorityById(23L);
        Assert.assertNotNull(authority);
    }

    @Test
    public void getAuthorityFail() {
        AuthorityDto authority = authorityService.getAuthorityById(30L);
        Assert.assertNull(authority);
    }

    @Test
    public void addAuthoritySuccess() {
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(AuthorityName.ROLE_TEST.toString());
        Assert.assertNotNull(authorityService.add(authority));
    }
}
