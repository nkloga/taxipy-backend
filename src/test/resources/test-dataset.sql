
--Driver with multiple orders, testing order flow
INSERT INTO DRIVER (id,  amount_of_seats, car_color, car_manufacturing_year, car_model, car_plate_number, license_number, drivers_photo_id)
VALUES(1,  5, 'blue', '2013', 'VW', '432DFG', '233113', null);

INSERT INTO USER (id, email, enabled, firstname, lastname, password, phonenumber, username, driver_id)
VALUES(2, 'test@test1', true, 'FirstName', 'LastName', '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', 'P-Number', 'test-user1', 1);

INSERT INTO DRIVER (id,  amount_of_seats, car_color, car_manufacturing_year, car_model, car_plate_number, license_number, drivers_photo_id)
VALUES(3,  5, 'blue', '2013', 'VW', '432DFG', '233113', null);

INSERT INTO USER (id, email, enabled, firstname, lastname, password, phonenumber, username, driver_id)
VALUES(4, 'test@test2', true, 'FirstName', 'LastName', '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', 'P-Number', 'test-user2', 3);

--Passenger with multiple orders, testing order flow
INSERT INTO PASSENGER(id,  birthdate, ccv, country, credit_card_number, expiration_date, location)
VALUES(5, '01.01.2000', 123, 'Estonia', 'CRED123412341234', '01.01.2024', 'Tallinn');

INSERT INTO USER (id, email, enabled, firstname, lastname, password, phonenumber, username, passenger_id)
VALUES(6, 'test@test3', true, 'FirstName', 'LastName', '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', 'P-Number', 'test-user3', 5);

--Passenger with single order, testing order flow
INSERT INTO PASSENGER(id,  birthdate, ccv, country, credit_card_number, expiration_date, location)
VALUES(7, '01.01.2000', 123, 'Estonia', 'CRED123412341235', '01.01.2024', 'Tallinn');

INSERT INTO USER (id, email, enabled, firstname, lastname, password, phonenumber, username, passenger_id)
VALUES(8, 'test@test4', true, 'FirstName', 'LastName', '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', 'P-Number', 'test-user4', 7);

--Passenger with no orders, testing order flow
INSERT INTO PASSENGER(id,  birthdate, ccv, country, credit_card_number, expiration_date, location)
VALUES(9, '01.01.2000', 123, 'Estonia', 'CRED123412341236', '01.01.2024', 'Tallinn');

INSERT INTO USER (id, email, enabled, firstname, lastname, password, phonenumber, username, passenger_id)
VALUES(10, 'test@test5', true, 'FirstName', 'LastName', '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', 'P-Number', 'test-user5', 9);

--status: CANCELLED, driver_id and passenger_id are present
INSERT INTO customer_order(id, cost, date, distance, from_lat, from_long, from_name, rating, status, time, where_lat, where_long, where_name, driver_id, passenger_id)
VALUES (11, 0.0, null, 0.0, '12', '13', 'tallinn', 1, 1, null, '14', '15', 'riga', 2, 6 );

--status: IN_PROGRESS, driver_id and passenger_id are present
INSERT INTO customer_order(id, cost, date, distance, from_lat, from_long, from_name, rating, status, time, where_lat, where_long, where_name, driver_id, passenger_id)
VALUES (12, 0.0, null, 0.0, '12', '13', 'tallinn', null, 0, null, '14', '15', 'riga', 2, 6 );

--status: STARTED, passenger_id is present
INSERT INTO customer_order(id, cost, date, distance, from_lat, from_long, from_name, rating, status, time, where_lat, where_long, where_name, driver_id, passenger_id)
VALUES (14, 16.0, '2019-10-02 21:16:01.949', 16.0, '12', '13', 'tallinn', 0, 3, null, '14', '15', 'riga', null, 6);

--status: IN_PROGRESS, passenger_id is present
INSERT INTO customer_order(id, cost, date, distance, from_lat, from_long, from_name, rating, status, time, where_lat, where_long, where_name, driver_id, passenger_id)
VALUES (15, 16.0, '2019-10-02 21:16:01.949', 16.0, '12', '13', 'tallinn', 0, 0, null, '14', '15', 'riga', null, 6);

--status: IN_PROGRESS, passenger_id is present
INSERT INTO customer_order(id, cost, date, distance, from_lat, from_long, from_name, rating, status, time, where_lat, where_long, where_name, driver_id, passenger_id)
VALUES (16, 0.0, '2019-10-02 21:16:01.949', 0.0, '12', '13', 'tallinn', 0, 0, null, '14', '15', 'riga', 4, 6);

--status: STARTED, driver_id and passenger_id are present
INSERT INTO customer_order(id, cost, date, distance, from_lat, from_long, from_name, rating, status, time, where_lat, where_long, where_name, driver_id, passenger_id)
VALUES (17, 0.0, '2019-10-02 21:16:01.949', 0.0, '12', '13', 'tallinn', null, 3, null, '14', '15', 'riga', 2, 6 );

--status: CANCELLED, driver_id and passenger_id are present
INSERT INTO customer_order(id, cost, date, distance, from_lat, from_long, from_name, rating, status, time, where_lat, where_long, where_name, driver_id, passenger_id)
VALUES (18, 0.0, null, 0.0, '12', '13', 'tallinn', null, 2, null, '14', '15', 'riga', 2, 6 );

--status: IN_PROGRESS, passenger_id is present
INSERT INTO customer_order(id, cost, date, distance, from_lat, from_long, from_name, rating, status, time, where_lat, where_long, where_name, driver_id, passenger_id)
VALUES (19, 0.0, null, 0.0, '12', '13', 'tallinn', null, 0, null, '14', '15', 'riga', null, 6 );

--status: COMPLETED, driver_id and passenger_id are present
INSERT INTO customer_order(id, cost, date, distance, from_lat, from_long, from_name, rating, status, time, where_lat, where_long, where_name, driver_id, passenger_id)
VALUES (20, 0.0, null, 0.0, '12', '13', 'tallinn', null, 5, null, '14', '15', 'riga', 2, 6 );

--status: STARTED, driver_id and passenger_id are present
INSERT INTO customer_order(id, cost, date, distance, from_lat, from_long, from_name, rating, status, time, where_lat, where_long, where_name, driver_id, passenger_id)
VALUES (21, 0.0, null, 0.0, '12', '13', 'tallinn', null, 3, null, '14', '15', 'riga', 2, 8 );

--status: CANCELLED, driver_id and passenger_id are present
INSERT INTO customer_order(id, cost, date, distance, from_lat, from_long, from_name, rating, status, time, where_lat, where_long, where_name, driver_id, passenger_id)
VALUES (22, 0.0, null, 0.0, '12', '13', 'tallinn', null, 2, null, '14', '15', 'riga', 2, 6 );

MERGE INTO AUTHORITY KEY (id) VALUES (23,'ROLE_PASSENGER');
MERGE INTO AUTHORITY KEY (id) VALUES (24,'ROLE_DRIVER');
MERGE INTO AUTHORITY KEY (id) VALUES (25,'ROLE_ADMIN');

--Passenger with one order, testing lost item
INSERT INTO PASSENGER(id,  birthdate, ccv, country, credit_card_number, expiration_date, location)
VALUES(29, '01.01.2000', 123, 'Estonia', 'CRED123412346666', '01.01.2024', 'Tallinn');

INSERT INTO USER (id, email, enabled, firstname, lastname, password, phonenumber, username, passenger_id)
VALUES(30, 'test@test8', true, 'FirstName', 'LastName', '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', 'P-Number', 'test-user8', 29);

--status: IN_PROGRESS, passenger_id is present
INSERT INTO customer_order(id, cost, date, distance, from_lat, from_long, from_name, rating, status, time, where_lat, where_long, where_name, driver_id, passenger_id)
VALUES (26, 16.0, '2019-10-02 21:16:01.949', 16.0, '12', '13', 'tallinn', 0, 6, null, '14', '15', 'riga', 2, 30);

INSERT INTO lost_item (id, description, status, type, order_id) VALUES (27, 'description', 0, 0, 26);

--Disabled user
INSERT INTO USER (id, email, enabled, firstname, lastname, password, phonenumber, username)
VALUES(28, 'test@test7', false, 'FirstName', 'LastName', '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', 'P-Number', 'test-user7');


INSERT INTO DRIVER (id,  amount_of_seats, car_color, car_manufacturing_year, car_model, car_plate_number, license_number, drivers_photo_id)
VALUES(31,  5, 'blue', '2013', 'VW', '432DFG', '111113', null);

INSERT INTO USER (id, email, enabled, firstname, lastname, password, phonenumber, username, driver_id)
VALUES(32, 'test@test9', true, 'FirstName', 'LastName', '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', 'P-Number', 'test-user9', 31);

--status: PAID, passenger_id is present
INSERT INTO customer_order(id, cost, date, distance, from_lat, from_long, from_name, rating, status, time, where_lat, where_long, where_name, driver_id, passenger_id)
VALUES (33, 16.0, '2019-10-02 21:16:01.949', 16.0, '12', '13', 'tallinn', 0, 6, null, '14', '15', 'riga', 2, 6);

INSERT INTO PASSENGER(id,  birthdate, ccv, country, credit_card_number, expiration_date, location)
VALUES(35, '01.01.2000', 123, 'Estonia', 'CRED123412346666', '01.01.2024', 'Tallinn');

INSERT INTO USER (id, email, enabled, firstname, lastname, password, phonenumber, username, passenger_id)
VALUES(36, 'blocked@test', false, 'FirstName', 'LastName', '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', 'P-Number', 'blocked', 35);

--Passenger with one order, testing lost item
INSERT INTO PASSENGER(id,  birthdate, ccv, country, credit_card_number, expiration_date, location)
VALUES(37, '01.01.2000', 123, 'Estonia', 'CRED123412341236', '01.01.2024', 'Tallinn');

INSERT INTO USER (id, email, enabled, firstname, lastname, password, phonenumber, username, passenger_id)
VALUES(38, 'test@test6', true, 'FirstName', 'LastName', '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', 'P-Number', 'test-user6', 37);


INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (2,24);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (4,24);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (6,23);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (8,23);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (10,23);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (28,23);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (30,23);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (32,24);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (36,23);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (38,23);



