package com.sda.practical.controller;

import com.sda.practical.exception.GeneralException;
import com.sda.practical.dto.UploadFileResponse;
import com.sda.practical.dto.UserDto;
import com.sda.practical.model.DBFile;
import com.sda.practical.model.Driver;
import com.sda.practical.model.User;
import com.sda.practical.security.JwtTokenUtil;
import com.sda.practical.service.DriverService;
import com.sda.practical.service.UserService;
import com.sda.practical.service.impl.DBFileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/user")
@RestController
public class UserController {

    private final UserService userService;
    private final DriverService driverService;
    private final DBFileStorageService DBFileStorageService;
    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    @Qualifier("jwtUserDetailsService")
    private UserDetailsService userDetailsService;

    @Autowired
    public UserController(UserService userService, DriverService driverService, DBFileStorageService dbFileStorageService) {
        this.userService = userService;
        this.driverService = driverService;
        DBFileStorageService = dbFileStorageService;
    }

    @ExceptionHandler(GeneralException.class)
    public ResponseEntity<String> userCheckExceptions(GeneralException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Run after succesful login to get current user profile info
     *
     * @return currently logged as dto object
     */
    @GetMapping(value = "")
    public UserDto getAuthenticatedUser(HttpServletRequest request) {
        logger.info("GET authenticated user");
        String token = request.getHeader(tokenHeader).substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        UserDto user = (UserDto) userDetailsService.loadUserByUsername(username);
        logger.info("Found user: " + user);
        return user;
    }

    /**
     * Registration for passenger or driver
     *
     * @param user object with sensitive info (pw, credit card info etc)
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> registerUser(@RequestBody User user) {
        logger.info("POST user: " + user);
        return ResponseEntity.ok(userService.register(user));
    }

    /**
     * Used to update user info in profile component
     *
     * @param user updated object
     * @return confirmation
     */
    @PostMapping(value = "/updateCurrentUser")
    public ResponseEntity<Boolean> updateUser(@RequestBody User user) {
        logger.info("POST updating user: " + user);
        return ResponseEntity.ok(userService.updateCurrentUser(user));
    }

    /**
     * Used by driver only to update profile picture
     *
     * @param file multipart photo file //TODO set file limits
     * @return URL of the file
     */
    @PostMapping("/uploadDriverPhoto")
    @PreAuthorize("hasRole('ROLE_DRIVER') or hasRole('ROLE_PASSENGER')")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        logger.info("POST uploading drivers photo file: " + file);
        DBFile dbFile = DBFileStorageService.storeFile(file);
        String fileDownloadUri = "/user/downloadDriverPhoto/" + dbFile.getId();
        Driver currentDriver = userService.getCurrentUser().getDriver();
        currentDriver.setDriversPhoto(dbFile);
        driverService.updateDriver(currentDriver);
        UploadFileResponse fileResponse = new UploadFileResponse(dbFile.getFileName(), fileDownloadUri, file.getContentType(), file.getSize());
        logger.info("File response: " + fileResponse);
        return fileResponse;
    }

    /**
     * Get file URL, used by driver and passenger when order is accepted
     *
     * @param fileId unique id
     * @return file
     * @throws Exception
     */
    @GetMapping("/downloadDriverPhoto/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileId) throws Exception {
        logger.info("GET Load file from database");
        DBFile dbFile = DBFileStorageService.getFile(fileId);
        ResponseEntity<Resource> response = ResponseEntity.ok().contentType(MediaType.parseMediaType(dbFile.getFileType())).header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"").body(new ByteArrayResource(dbFile.getData()));
        logger.info("Got response: " + response);
        return response;
    }

    /**
     * Can be used by driver to download own photo for the profile component
     *
     * @return URL
     * @throws Exception
     */
    @GetMapping("/downloadDriverPhoto")
    public UploadFileResponse downloadProfilePhoto() throws Exception {
        UploadFileResponse response = new UploadFileResponse(null, null, null, 0L);
        logger.info("GET Load file from database");
        Driver driver = userService.getCurrentUser().getDriver();
        if (driver != null) {
            if (driver.getDriversPhoto() != null) {
                DBFile dbFile = DBFileStorageService.getFile(driver.getDriversPhoto().getId());
                String fileDownloadUri = "/user/downloadDriverPhoto/" + dbFile.getId();
                UploadFileResponse uploadFileResponse = new UploadFileResponse(dbFile.getFileName(), fileDownloadUri, null, 0L);
                logger.info("Got response: " + uploadFileResponse);
                return uploadFileResponse;
            }
            return response;
        } else return response;
    }
}