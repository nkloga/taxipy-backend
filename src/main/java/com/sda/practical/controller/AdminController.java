package com.sda.practical.controller;

import com.sda.practical.dto.AuthUserDto;
import com.sda.practical.dto.DriverDto;
import com.sda.practical.dto.PassengerDto;
import com.sda.practical.dto.UserDto;
import com.sda.practical.service.DriverService;
import com.sda.practical.service.PassengerService;
import com.sda.practical.service.UserService;
import com.sda.practical.service.impl.AuthorityServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RequestMapping("/admin")
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RestController
public class AdminController {

    private final UserService userService;
    private final ModelMapper modelMapper;
    private final DriverService driverService;
    private final PassengerService passengerService;
    private final Logger logger = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    public AdminController(UserService userService, ModelMapper modelMapper, DriverService driverService, PassengerService passengerService) {
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.driverService = driverService;
        this.passengerService = passengerService;
    }

    /**
     * @param id User id
     * @return blocked user instance used in profile component by admin
     */
    @RequestMapping(value = "/block-user/{id}", method = RequestMethod.GET)
    public AuthUserDto blockUser(@PathVariable Long id) {
        logger.info("GET request to block a user with id: " + id);
        return userService.blockUser(id, false);
    }

    /**
     * @param id User id
     * @return unblocked user instance used in profile component by admin
     */
    @RequestMapping(value = "/unblock-user/{id}", method = RequestMethod.GET)
    public AuthUserDto unblockUser(@PathVariable Long id) {
        logger.info("GET request to unblock a user with id: " + id);
        return userService.blockUser(id, true);
    }

    /**
     * @return list of users, used for blocking unblocking by admin in profile component
     */
    @GetMapping(value = "/list")
    public ResponseEntity<List<AuthUserDto>> getUserList() {
        logger.info("GET request to list all registered users");
        return ResponseEntity.ok(userService.getListOfUsers());
    }
}