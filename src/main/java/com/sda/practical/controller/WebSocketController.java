package com.sda.practical.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Web socket is used to exchange order information between driver and passenger in a real time
 */
@Controller
public class WebSocketController {

    private static final String SENDING_URL = "/topic/server-broadcaster";
    private static final String RECEIVING_URL = "/server-receiver";
    private final SimpMessagingTemplate template;
    private AtomicLong counter = new AtomicLong(0);
    private String message = "";
    private final Logger logger = LoggerFactory.getLogger(WebSocketController.class);

    @Autowired
    public WebSocketController(SimpMessagingTemplate template) {
        this.template = template;
    }

    @MessageMapping(RECEIVING_URL)
    public void onReceivedMessage(String message) {
        logger.info("New message receiver: " + message);
        template.convertAndSend(SENDING_URL, message);
    }

    @SubscribeMapping(SENDING_URL)
    public String onSubscribe() {
        return message;
    }
}
