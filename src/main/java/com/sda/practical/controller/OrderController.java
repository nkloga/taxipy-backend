package com.sda.practical.controller;

import com.sda.practical.exception.GeneralException;
import com.sda.practical.dto.OrderDto;
import com.sda.practical.service.impl.OrderServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * HATEOAS URLs are used to control order workflow
 * returned object has new URLs which are parsed by front end and menu is updated according to new URLs
 */
@RestController
public class OrderController {
    private final OrderServiceImpl orderService;
    private final Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    public OrderController(OrderServiceImpl orderService) {
        this.orderService = orderService;
    }

    /**
     * Used by driver to see all open orders
     *
     * @return list of open orders
     */
    @PreAuthorize("hasRole('ROLE_DRIVER')")
    @RequestMapping(value = "/orders/open", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Resources<Resource<OrderDto>>> getAllOpenOrders() {
        logger.info("GET list of open orders");
        Resources<Resource<OrderDto>> list = orderService.getAllOpenOrders();
        logger.info("Got list: " + list);
        return ResponseEntity.ok(list);
    }

    /**
     * Used by passenger/driver profile component to show orders history
     *
     * @return list of non active orders of current user
     */
    @PreAuthorize("hasRole('ROLE_DRIVER') or hasRole('ROLE_PASSENGER') or hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/orders/history", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Resources<Resource<OrderDto>>> getAllMyOrders() {
        logger.info("GET list of historical orders of current users");
        Resources<Resource<OrderDto>> list = orderService.getAllHistoryOrders();
        logger.info("Got list: " + list);
        return ResponseEntity.ok(list);
    }

    /**
     * Used by driver/passenger at the application start to update menu based on last open order
     *
     * @return last order of current user
     */
    @PreAuthorize("hasRole('ROLE_DRIVER') or hasRole('ROLE_PASSENGER')")
    @RequestMapping(value = "/orders/last-open", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Resource<OrderDto>> getLastOpenOrder() {
        logger.info("GET last open order");
        Resource<OrderDto> order = orderService.getLastOpenOrder();
        logger.info("Got order: " + order);
        return ResponseEntity.ok(order);
    }

    @PreAuthorize("hasRole('ROLE_DRIVER') or hasRole('ROLE_PASSENGER') or hasRole('ROLE_ADMIN') ")
    @RequestMapping(value = "/orders/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Resource<OrderDto>> one(@PathVariable Long id) {
        logger.info("GET order by id");
        Resource<OrderDto> order = orderService.getOrderDtoById(id);
        logger.info("Got order: " + order);
        return ResponseEntity.ok(order);
    }

    /**
     * Create order, can be done only by passenger
     *
     * @param orderDto order with where/from coordinates and location names
     * @return order dto with next available actions
     */
    @PreAuthorize("hasRole('ROLE_PASSENGER')")
    @RequestMapping(value = "/orders/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Resource<OrderDto>> add(@RequestBody OrderDto orderDto) {
        logger.info("POST Adding a new order: " + orderDto);
        return ResponseEntity.ok(orderService.add(orderDto));
    }

    /**
     * @param id of the order
     * @return order dto with next available actions
     */
    @PreAuthorize("hasRole('ROLE_PASSENGER')") // use patch
    @RequestMapping(value = "/orders/{id}/cancel", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Resource<OrderDto>> cancel(@PathVariable Long id) {
        logger.info("GET Canceling an order with id : " + id);
        return ResponseEntity.ok(orderService.cancel(id));
    }

    @PreAuthorize("hasRole('ROLE_DRIVER')") // use patch
    @RequestMapping(value = "/orders/{id}/start", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Resource<OrderDto>> start(@PathVariable Long id) {
        logger.info("GET Canceling an order with id : " + id);
        return ResponseEntity.ok(orderService.start(id));
    }

    @PreAuthorize("hasRole('ROLE_DRIVER')")
    @RequestMapping(value = "/orders/{id}/terminate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Resource<OrderDto>> terminate(@PathVariable Long id) {
        logger.info("GET Terminating an order with id : " + id);
        return ResponseEntity.ok(orderService.terminate(id));
    }

    @PreAuthorize("hasRole('ROLE_PASSENGER')")
    @RequestMapping(value = "/orders/{id}/pay", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Resource<OrderDto>> pay(@PathVariable Long id) {
        logger.info("GET Paying for an order with id : " + id);
        return ResponseEntity.ok(orderService.pay(id));
    }

    @PreAuthorize("hasRole('ROLE_DRIVER')")
    @RequestMapping(value = "/orders/{id}/complete", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResourceSupport> complete(@PathVariable Long id) {
        logger.info("GET Completing an order with id : " + id);
        return ResponseEntity.ok(orderService.complete(id));
    }

    @PreAuthorize("hasRole('ROLE_DRIVER')")
    @RequestMapping(value = "/orders/{id}/accept", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResourceSupport> accept(@PathVariable Long id) {
        logger.info("GET Accepting an order with id : " + id);
        return ResponseEntity.ok(orderService.accept(id));
    }

    @PreAuthorize("hasRole('ROLE_PASSENGER')")
    @RequestMapping(value = "/orders/{id}/feedback", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResourceSupport> feedback(@PathVariable Long id, @RequestBody Integer rating) {
        logger.info("GET Setting feedback an order with id : " + id);
        return ResponseEntity.ok(orderService.feedback(id, rating));
    }

    @ExceptionHandler(GeneralException.class)
    public ResponseEntity<String> orderCheckExceptions(GeneralException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
