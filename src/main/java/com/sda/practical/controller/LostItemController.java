package com.sda.practical.controller;

import com.sda.practical.dto.LostItemDto;
import com.sda.practical.dto.OrderDto;
import com.sda.practical.service.LostItemService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Used by driver or passenger to submit found or lost item and admin to resolve the case
 */




//@RequestMapping("/lost")
@RestController
@EnableWebMvc

public class LostItemController {

    private final LostItemService lostItemService;
    private final ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(LostItemController.class);

    @Autowired
    public LostItemController(LostItemService lostItemService, ModelMapper modelMapper) {
        this.lostItemService = lostItemService;
        this.modelMapper = modelMapper;
    }

    /**
     * @return list of all lost and found items
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/lost/list")
    public ResponseEntity<Resources<LostItemDto>> getListOfLostItems() {
        logger.info("GET Requesting list of lost/found items");
        Resources<LostItemDto> list = lostItemService.getListOfLostItems();
        logger.info("Received list: " + list);
        return ResponseEntity.ok(list);
    }

    /**
     * Admin can click on any item on the list and set it to resolved,
     * current functionality assumes that admin will contact driver or passenger via the phone
     *
     * @param id of an item
     * @return saved itemDto
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/lost/close/{id}")
    public ResponseEntity<LostItemDto> close(@PathVariable Long id) {
        logger.info("GET closing a case with item id: " + id);
        return ResponseEntity.ok(lostItemService.close(id));
    }

    /**
     * Driver or passenger can submit an item
     *
     * @param lostItemDto from passenger or driver
     * @return saved instance
     */
    @PreAuthorize("hasRole('ROLE_DRIVER') or hasRole('ROLE_PASSENGER')")
    @RequestMapping(value = "/lost/submit", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LostItemDto> submitLostItem(@RequestBody LostItemDto lostItemDto) {
        logger.info("POST submitting a new item: " + lostItemDto);
        return ResponseEntity.ok(lostItemService.submitLostItem(lostItemDto));
    }
}

