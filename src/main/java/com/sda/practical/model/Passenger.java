package com.sda.practical.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "PASSENGER")
public class Passenger {

    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(name = "sequence-generator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {@org.hibernate.annotations.Parameter(name = "sequence_name", value = "user_sequence"), @org.hibernate.annotations.Parameter(name = "initial_value", value = "1000"), @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")})
    private Long id;

    @OneToOne(targetEntity = User.class, mappedBy = "passenger")
    private User user;

    @Column(name = "BIRTHDATE", length = 50)
    @NotNull
    @Size(min = 4, max = 50)
    private String birthdate;

    @Column(name = "CREDIT_CARD_NUMBER", length = 16)
    @NotNull
    @Size(min = 16, max = 16)
    private String creditCardNumber; //TODO keep in separate table

    @Column(name = "CCV")
    @NotNull
    private Integer ccv;

    @Column(name = "EXPIRATION_DATE", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    private String expirationDate;

    @Column(name = "LOCATION", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    private String location;

    @Column(name = "COUNTRY", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    private String country;

    public Integer getCcv() {
        return ccv;
    }

    public void setCcv(Integer ccv) {
        this.ccv = ccv;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

}
