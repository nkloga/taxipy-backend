package com.sda.practical.model;

import org.hibernate.annotations.GenericGenerator;

import javax.validation.constraints.NotNull;
import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "DRIVER")
public class Driver {

    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(name = "sequence-generator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {@org.hibernate.annotations.Parameter(name = "sequence_name", value = "user_sequence"), @org.hibernate.annotations.Parameter(name = "initial_value", value = "1000"), @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")})
    private Long id;

    @Column(name = "LICENSE_NUMBER", length = 10)
    @NotNull
    @Size(min = 4, max = 10)
    private String licenseNumber;

    @OneToOne(targetEntity = DBFile.class)
    private DBFile driversPhoto;

    @Column(name = "CAR_PLATE_NUMBER", length = 10)
    @NotNull
    @Size(min = 4, max = 10)
    private String carPlateNumber;

    @Column(name = "CAR_MANUFACTURING_YEAR")
    @NotNull
    private String carManufacturingYear;

    @Column(name = "CAR_MODEL", length = 20)
    @NotNull
    @Size(min = 2, max = 20)
    private String carModel;

    @Column(name = "AMOUNT_OF_SEATS")
    @NotNull
    private Integer amountOfSeats; //TODO change to integer

    @Column(name = "CAR_COLOR", length = 20)
    @NotNull
    @Size(min = 1, max = 20)
    private String carColor;

    @OneToOne(targetEntity = User.class, mappedBy = "driver")
    private User user;

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public Integer getAmountOfSeats() {
        return amountOfSeats;
    }

    public void setAmountOfSeats(Integer amountOfSeats) {
        this.amountOfSeats = amountOfSeats;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public DBFile getDriversPhoto() {
        return driversPhoto;
    }

    public void setDriversPhoto(DBFile driversPhoto) {
        this.driversPhoto = driversPhoto;
    }

    public String getCarPlateNumber() {
        return carPlateNumber;
    }

    public void setCarPlateNumber(String carPlateNumber) {
        this.carPlateNumber = carPlateNumber;
    }

    public String getCarManufacturingYear() {
        return carManufacturingYear;
    }

    public void setCarManufacturingYear(String carManufacturingYear) {
        this.carManufacturingYear = carManufacturingYear;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
