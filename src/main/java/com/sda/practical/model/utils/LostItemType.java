package com.sda.practical.model.utils;

public enum LostItemType {
    LOST, FOUND
}