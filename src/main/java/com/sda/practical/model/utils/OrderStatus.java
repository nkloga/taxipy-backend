package com.sda.practical.model.utils;

public enum OrderStatus {
    IN_PROGRESS,
    CANCELLED,
    ACCEPTED,
    STARTED,
    TERMINATED,
    COMPLETED,
    PAID
}
