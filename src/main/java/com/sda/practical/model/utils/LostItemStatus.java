package com.sda.practical.model.utils;

public enum LostItemStatus {
    SUBMITTED, RETURNED, CLOSED
}