package com.sda.practical.model.utils;

public enum AuthorityName {
    ROLE_PASSENGER, ROLE_DRIVER, ROLE_ADMIN, ROLE_TEST
}