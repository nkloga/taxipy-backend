package com.sda.practical.model;

import com.sda.practical.model.utils.OrderStatus;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "CUSTOMER_ORDER")
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class Order {

    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(name = "sequence-generator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {@org.hibernate.annotations.Parameter(name = "sequence_name", value = "user_sequence"), @org.hibernate.annotations.Parameter(name = "initial_value", value = "1000"), @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")})
    private Long id;

    @Column(name = "FROM_LAT", length = 20)
    @NotNull
    @Size(min = 1, max = 20)
    private String fromLat;

    @Column(name = "FROM_LONG", length = 20)
    @NotNull
    @Size(min = 1, max = 20)
    private String fromLong;

    @ManyToOne
    private User driver;

    @ManyToOne
    private User passenger;

    @Column(name = "STATUS")
    @NotNull
    @Enumerated
    private OrderStatus status;


    @Column(name = "COST")
    private Double cost;


    @Column(name = "DISTANCE")
    private Double distance;

    @Column(name = "TIME")
    private Long time;

    @Column(name = "DATE")
    private Date date;

    @Column(name = "WHERE_LAT", length = 20)
    @NotNull
    @Size(min = 1, max = 20)
    private String whereLat;

    @Column(name = "WHERE_LONG", length = 20)
    @NotNull
    @Size(min = 1, max = 20)
    private String whereLong;

    @Column(name = "WHERE_NAME", length = 50)
    @Size(min = 1, max = 50)
    private String whereName;

    @Column(name = "FROM_NAME", length = 50)
    @Size(min = 1, max = 50)
    private String fromName;

    @Column(name = "RATING")
    private Integer rating;

    public static Order of(String fromLat, String whereLat, String fromName, String fromLong, String whereLong, String whereName, User driver, User passenger, Double cost, Double distance, Long time) {
        Order order = new Order();
        order.fromLat = fromLat;
        order.fromLong = fromLong;
        order.whereLat = whereLat;
        order.whereLong = whereLong;
        order.whereName = whereName;
        order.fromName = fromName;
        order.driver = driver;
        order.passenger = passenger;
        order.cost = cost;
        order.distance = distance;
        order.time = time;
        order.date = new Date();
        order.status = OrderStatus.IN_PROGRESS;
        return order;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFromLat() {
        return fromLat;
    }

    public void setFromLat(String fromLat) {
        this.fromLat = fromLat;
    }

    public String getFromLong() {
        return fromLong;
    }

    public void setFromLong(String fromLong) {
        this.fromLong = fromLong;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

    public User getPassenger() {
        return passenger;
    }

    public void setPassenger(User passenger) {
        this.passenger = passenger;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getWhereLat() {
        return whereLat;
    }

    public void setWhereLat(String whereLat) {
        this.whereLat = whereLat;
    }

    public String getWhereLong() {
        return whereLong;
    }

    public void setWhereLong(String whereLong) {
        this.whereLong = whereLong;
    }

    public String getWhereName() {
        return whereName;
    }

    public void setWhereName(String whereName) {
        this.whereName = whereName;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
