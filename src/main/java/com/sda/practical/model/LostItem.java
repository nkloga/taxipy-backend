package com.sda.practical.model;

import com.sda.practical.model.utils.LostItemStatus;
import com.sda.practical.model.utils.LostItemType;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "LOST_ITEM")
public class LostItem {
    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(name = "sequence-generator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {@org.hibernate.annotations.Parameter(name = "sequence_name", value = "user_sequence"), @org.hibernate.annotations.Parameter(name = "initial_value", value = "1000"), @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")})
    private Long id;

    @Column(name = "DESCRIPTION", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    private String description;

    @Column(name = "TYPE")
    @NotNull
    private LostItemType type;

    @ManyToOne(targetEntity = Order.class)
    private Order order;

    @Column(name = "STATUS")
    @NotNull
    private LostItemStatus status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LostItemType getType() {
        return type;
    }

    public void setType(LostItemType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public LostItemStatus getStatus() {
        return status;
    }

    public void setStatus(LostItemStatus status) {
        this.status = status;
    }
}
