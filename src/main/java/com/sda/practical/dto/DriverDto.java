package com.sda.practical.dto;

import com.sda.practical.model.DBFile;

public class DriverDto {
    private Long id;
    private String licenseNumber;
    private String driversPhoto;
    private String carPlateNumber;
    private String carManufacturingYear;
    private String carModel;
    private Integer amountOfSeats;
    private String carColor;

    public DriverDto(String licenseNumber, DBFile driversPhoto, String carPlateNumber, String carManufacturingYear, String carModel, Integer amountOfSeats, String carColor) {
        this.licenseNumber = licenseNumber;
        this.carPlateNumber = carPlateNumber;
        this.carManufacturingYear = carManufacturingYear;
        this.carModel = carModel;
        this.amountOfSeats = amountOfSeats;
        this.carColor = carColor;
    }

    public DriverDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getDriversPhoto() {
        return driversPhoto;
    }

    public void setDriversPhoto(String driversPhoto) {
        this.driversPhoto = driversPhoto;
    }

    public String getCarPlateNumber() {
        return carPlateNumber;
    }

    public void setCarPlateNumber(String carPlateNumber) {
        this.carPlateNumber = carPlateNumber;
    }

    public String getCarManufacturingYear() {
        return carManufacturingYear;
    }

    public void setCarManufacturingYear(String carManufacturingYear) {
        this.carManufacturingYear = carManufacturingYear;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public Integer getAmountOfSeats() {
        return amountOfSeats;
    }

    public void setAmountOfSeats(Integer amountOfSeats) {
        this.amountOfSeats = amountOfSeats;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

}
