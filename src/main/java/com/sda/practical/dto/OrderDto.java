package com.sda.practical.dto;

import com.sda.practical.model.utils.OrderStatus;
import org.springframework.hateoas.ResourceSupport;
import java.util.Date;

public class OrderDto extends ResourceSupport {

    private Long orderId;
    private OrderStatus status;
    private String fromLat;
    private String whereLat;
    private String fromLong;
    private String whereLong;
    private String whereName;
    private String fromName;
    private Double cost;
    private Double distance;
    private Long time;
    private Date date;
    private Integer rating;
    private UserDto driver;
    private UserDto passenger;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setId(Long orderId) {
        this.orderId = orderId;
    }

    public String getWhereName() {
        return whereName;
    }

    public void setWhereName(String whereName) {
        this.whereName = whereName;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public String getFromLat() {
        return fromLat;
    }

    public void setFromLat(String fromLat) {
        this.fromLat = fromLat;
    }

    public String getWhereLat() {
        return whereLat;
    }

    public void setWhereLat(String whereLat) {
        this.whereLat = whereLat;
    }

    public String getFromLong() {
        return fromLong;
    }

    public void setFromLong(String fromLong) {
        this.fromLong = fromLong;
    }

    public String getWhereLong() {
        return whereLong;
    }

    public void setWhereLong(String whereLong) {
        this.whereLong = whereLong;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public UserDto getDriver() {
        return driver;
    }

    public void setDriver(UserDto driver) {
        this.driver = driver;
    }

    public UserDto getPassenger() {
        return passenger;
    }

    public void setPassenger(UserDto passenger) {
        this.passenger = passenger;
    }
}
