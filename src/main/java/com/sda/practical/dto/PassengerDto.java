package com.sda.practical.dto;

public class PassengerDto {
    private Long id;
    private String birthdate;
    private String creditCardNumber; //TODO keep in separate table
    private Integer ccv;
    private String expirationDate;
    private String location;
    private String country;

    public PassengerDto(String birthdate, String creditCardNumber, Integer ccv, String expirationDate, String location, String country) {
        this.birthdate = birthdate;
        this.creditCardNumber = creditCardNumber;
        this.ccv = ccv;
        this.expirationDate = expirationDate;
        this.location = location;
        this.country = country;
    }

    public PassengerDto() {
    }

    public Integer getCcv() {
        return ccv;
    }

    public void setCcv(Integer ccv) {
        this.ccv = ccv;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
