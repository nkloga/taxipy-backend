package com.sda.practical.dto;

import com.sda.practical.model.utils.LostItemStatus;
import com.sda.practical.model.utils.LostItemType;

public class LostItemDto {
    private Long id;
    private String description;
    private LostItemType type;
    private LostItemStatus status;



    private Long order;

//    public LostItemDto(String description, Long orderId) {
//        this.description = description;
//        this.orderId = orderId;
//        this.status = LostItemStatus.SUBMITTED;
//    }

//    public LostItemDto() {
//    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LostItemType getType() {
        return type;
    }


    public void setType(LostItemType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LostItemStatus getStatus() {
        return status;
    }

    public void setStatus(LostItemStatus status) {
        this.status = status;
    }
}
