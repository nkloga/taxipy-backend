package com.sda.practical.dto;

import com.sda.practical.model.utils.AuthorityName;

import java.io.Serializable;

public class AuthorityDto implements Serializable {
    private AuthorityName authority;
    private Long id;

    public AuthorityDto(AuthorityName authority) {
        this.authority = authority;
    }

    public AuthorityDto() {
    }

    public AuthorityName getAuthority() {
        return authority;
    }

    public void setAuthority(AuthorityName name) {
        this.authority = authority;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
