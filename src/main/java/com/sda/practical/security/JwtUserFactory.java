package com.sda.practical.security;

import com.sda.practical.dto.DriverDto;
import com.sda.practical.dto.PassengerDto;
import com.sda.practical.dto.UserDto;
import com.sda.practical.model.Authority;
import com.sda.practical.model.Driver;
import com.sda.practical.model.Passenger;
import com.sda.practical.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;


import java.util.List;
import java.util.stream.Collectors;

/**
 * Factory class to convert user into DTO object and set authorities
 */
public final class JwtUserFactory {

    private JwtUserFactory() {
    }

    public static UserDto create(User user) {
        DriverDto driverDto = null;
        Driver driver = user.getDriver();
        if (driver != null) {
            driverDto = new DriverDto(
                    driver.getLicenseNumber(),
                    driver.getDriversPhoto(),
                    driver.getCarPlateNumber(),
                    driver.getCarManufacturingYear(),
                    driver.getCarModel(),
                    driver.getAmountOfSeats(),
                    driver.getCarColor());
        }
        PassengerDto passengerDto = null;
        Passenger passenger = user.getPassenger();

        if (passenger != null) {
            passengerDto = new PassengerDto(
                    passenger.getBirthdate(),
                    passenger.getCreditCardNumber(),
                    passenger.getCcv(),
                    passenger.getExpirationDate(),
                    passenger.getLocation(),
                    passenger.getCountry());
        }

        return new UserDto(
                user.getId(),
                user.getUsername(),
                user.getFirstname(),
                user.getLastname(),
                user.getEmail(),
                user.getPassword(),
                mapToGrantedAuthorities(user.getAuthorities()),
                user.getEnabled(),
                user.getLastPasswordResetDate(),
                driverDto,
                passengerDto,
                user.getPhoneNumber());
    }

    public static List<GrantedAuthority> mapToGrantedAuthorities(List<Authority> authorities) {
        return authorities.stream().map(authority -> new SimpleGrantedAuthority(authority.getAuthority().name())).collect(Collectors.toList());
    }
}
