package com.sda.practical.service;

import com.sda.practical.dto.LostItemDto;
import org.springframework.hateoas.Resources;

public interface LostItemService {

    /**
     * Gets a list of lost/found items reported by passenger or driver
     *
     * @return return saved profile
     */
    Resources<LostItemDto> getListOfLostItems();

    /**
     * Saves a lost item, set status
     *
     * @param lostItemDto lost or found items model
     * @return return dto of saved item
     */
    LostItemDto submitLostItem(LostItemDto lostItemDto);

    /**
     * Gets an item by id
     *
     * @param id ID of an item
     * @return return dto of saved item
     */
    LostItemDto getOne(Long id);

    /**
     * Resloves an open case about lost or found item
     *
     * @param id of lost or found item
     * @return return dto of saved item
     */
    LostItemDto close(Long id);
}
