package com.sda.practical.service;

import com.sda.practical.model.Passenger;

public interface PassengerService {

    /**
     * Saving passenger after checking if same credit card number is already in the database
     *
     * @param passenger Save passenger profile
     * @return return saved profile
     */
    Passenger savePassenger(Passenger passenger);

    /**
     * Updating passenger profile
     *
     * @param passenger Updated passenger profile
     * @return return saved profile
     */
    Passenger updatePassenger(Passenger passenger);

    /**
     * Get user by id
     *
     * @param id Id of the user
     * @return return saved profile
     */
    Passenger getOne(Long id);
}


