package com.sda.practical.service;

import com.sda.practical.dto.AuthUserDto;
import com.sda.practical.dto.UserDto;
import com.sda.practical.model.User;

import java.util.List;

public interface UserService {

    /**
     * Used by admins to enable/disable any driver or passenger
     *
     * @return List<AuthUserDto> returns list of all users excluding admins
     */
    List<AuthUserDto> getListOfUsers();

    /**
     * Gets user by username
     *
     * @return user model
     */
    User findByUserName(String username);

    /**
     * Used to create user account
     *
     * @param user Profile data provided by the user, including password, therefore standard UserDto was not used
     * @return saved user model if registration was successful
     */
    UserDto register(User user);

    /**
     * Gets a DTO instance of authenticated user and will be used in the the front end
     *
     * @return authUserDto
     */
    AuthUserDto authenticate();

    /**
     * Used by all users to update their personal profile information
     *
     * @param user A model with updated information that user wants to save in the db
     * @return confirmation if upload was successful
     */
    boolean updateCurrentUser(User user);

    /**
     * @return currently authenticated users profile
     */
    User getCurrentUser();

    /**
     * Used by admin to block or unblock any user
     *
     * @param id    User id
     * @param block Indicated an action, true if user should be enabled and false if disabled
     * @return saved user profile
     */
    AuthUserDto blockUser(Long id, boolean block);
}
