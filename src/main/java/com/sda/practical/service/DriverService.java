package com.sda.practical.service;

import com.sda.practical.dto.AuthUserDto;
import com.sda.practical.dto.DriverDto;
import com.sda.practical.dto.UserDto;
import com.sda.practical.model.Driver;

public interface DriverService {

    /**
     * Saving driver after checking if same driving license number is already in the database
     *
     * @param driver Save drivers profile
     * @return return saved profile
     */
    Driver saveDriver(Driver driver);

    /**
     * Updating drivers profile
     *
     * @param driver Updated drivers profile
     * @return return saved profile
     */
    Driver updateDriver(Driver driver);

    /**
     * Get driver by id
     *
     * @param id of a driver
     */
    Driver getOne(Long id);
}
