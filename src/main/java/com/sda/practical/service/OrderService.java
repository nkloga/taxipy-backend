package com.sda.practical.service;

import com.sda.practical.dto.OrderDto;
import com.sda.practical.exception.GeneralException;
import com.sda.practical.model.Order;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;

public interface OrderService {

    /**
     * Create order
     *
     * @param orderDto Submitted by the passenger in front end
     * @return Resource orderDto, HATEOAS links to available methods will be used on the front end to indicate possible actions for passenger or driver
     * @throws GeneralException with invalid order if order has not passed the validation
     */
    Resource<OrderDto> add(OrderDto orderDto);

    /**
     * Get order by id
     *
     * @param id Of the order
     * @return Resource orderDto, HATEOAS links to available methods will be used on the front end to indicate possible actions for passenger or driver
     * @throws GeneralException if order is not found
     */
    Resource<OrderDto> getOrderDtoById(Long id);

    /**
     * Not used by controllers, goal is to reduce amount of convertions to DTO and back
     */
    Order getOrderById(Long id);

    /**
     * Cancel an order
     * If status - in progress and there is no driver in the order, set null cost
     * If order is accepted or started - minimum cost that passenger should pay is 50% of estimation
     * Check if current order status allows to cancel or throw exception
     *
     * @param id ID of the order
     * @return Resource orderDto, HATEOAS links to available methods will be used on the front end to indicate possible actions for passenger or driver
     * @throws GeneralException if order cannot be cancelled or not found
     */
    Resource<OrderDto> cancel(Long id);

    /**
     * Complete an order
     * Check if order is not started - it cannot be completed
     * Calculate time of the ride
     *
     * @param id ID of the order
     * @return Resource orderDto, HATEOAS links to available methods will be used on the front end to indicate possible actions for passenger or driver
     * @throws GeneralException if order cannot be completed or not found
     */
    Resource<OrderDto> complete(Long id);

    /**
     * Accept an order
     * Check if order has already assigned driver or its status is different from in progress(just created)
     * Set current user as a driver
     *
     * @param id ID of the order
     * @return Resource orderDto, HATEOAS links to available methods will be used on the front end to indicate possible actions for passenger or driver
     * @throws GeneralException if order cannot be accepted or not found
     */
    Resource<OrderDto> accept(Long id);

    /**
     * Start a ride, after driver has arrived to the customer, he will start a ride
     * Check if order status is different from accepted
     * Set current time
     *
     * @param id ID of the order
     * @return Resource orderDto, HATEOAS links to available methods will be used on the front end to indicate possible actions for passenger or driver
     * @throws GeneralException if order cannot be started or not found
     */
    Resource<OrderDto> start(Long id);

    /**
     * Terminate a ride, if something happened and passenger dont have to pay
     * Check if order status is different from accepted, completed or started
     * Set null cost
     *
     * @param id ID of the order
     * @return Resource orderDto, HATEOAS links to available methods will be used on the front end to indicate possible actions for passenger or driver
     * @throws GeneralException if order cannot be terminated or not found
     */
    Resource<OrderDto> terminate(Long id);

    /**
     * Pay for a ride, done by passenger obviously
     * Check if order status is different from completed
     *
     * @param id ID of the order
     * @return Resource orderDto, HATEOAS links to available methods will be used on the front end to indicate possible actions for passenger or driver
     * @throws GeneralException if order cannot be paid or not found
     */
    Resource<OrderDto> pay(Long id);

    /**
     * Set passenger feedback
     * from 1 to 5
     *
     * @param id ID of the order
     * @return Resource orderDto, HATEOAS links to available methods will be used on the front end to indicate possible actions for passenger or driver
     * @throws GeneralException if order is not found
     */
    Resource<OrderDto> feedback(Long id, Integer rating);

    /**
     * Gives a list of open orders for the driver, which he can accept
     */
    Resources<Resource<OrderDto>> getAllOpenOrders();

    /**
     * Get last order fo the current user
     * Used to fetch latest status from db after getting notification about order change
     * There cannot be multiple open orders
     *
     * @return Resource orderDto, HATEOAS links to available methods will be used on the front end to indicate possible actions for passenger or driver
     * @throws GeneralException if order is not found
     */
    Resource<OrderDto> getLastOpenOrder();

    /**
     * Get list of closed/cancelled or terminated orders,
     * used in user profile and to submit lost or found item
     *
     * @return list of orderDto's
     * @throws GeneralException if current user is not driver or passenger
     */
    Resources<Resource<OrderDto>> getAllHistoryOrders();
}
