package com.sda.practical.service.order;

import com.sda.practical.dto.OrderDto;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


public class OrderValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return OrderDto.class.equals(clazz);
    }

    /**
     * Validates user for error from front end side
     *
     * @param target - order
     * @param errors  - to set error and message
     */
    @Override
    public void validate(Object target, Errors errors) {
        OrderDto order = (OrderDto) target;

        if (order.getFromLat() == null) errors.rejectValue("fromLat", "From latitude cannot be null");

        if (order.getFromLong() == null) errors.rejectValue("fromLong", "From longitude cannot be null");
        if (order.getWhereLat() == null) errors.rejectValue("whereLat", "Where latitude cannot be null");

        if (order.getWhereLong() == null) errors.rejectValue("whereLong", "Where longitude cannot be null");
        }
}