package com.sda.practical.service.order;

import com.sda.practical.dto.DriverDto;
import com.sda.practical.dto.UserDto;
import com.sda.practical.model.Authority;
import com.sda.practical.model.utils.AuthorityName;
import com.sda.practical.model.User;
import com.sda.practical.dto.OrderDto;
import com.sda.practical.model.Order;
import com.sda.practical.model.utils.OrderStatus;
import com.sda.practical.controller.OrderController;
import com.sda.practical.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Slf4j
@Component
public class OrderResourceAssembler implements ResourceAssembler<Order, Resource<OrderDto>> {

    private final Logger logger = LoggerFactory.getLogger(OrderResourceAssembler.class);

    @Autowired
    private final UserService userService;

    public OrderResourceAssembler(UserService userService) {
        this.userService = userService;
    }

    /**
     * Converts order to dto object, changes HATEOAS links depending on status and roles
     */
    @Override
    public Resource<OrderDto> toResource(Order order) {
        logger.info("Converting to DTO object following order: " + order);
        User loggedInUser = userService.findByUserName(userService.authenticate().getUsername());
        logger.info("Found currently authorized user: " + loggedInUser);
        List<AuthorityName> authorities = loggedInUser.getAuthorities().stream().map(Authority::getAuthority).collect(Collectors.toList());
        logger.info("User authorities: " + authorities);
        OrderDto orderDto = new OrderDto();
        orderDto.setId(order.getId());
        orderDto.setFromLat(order.getFromLat());
        orderDto.setWhereLat(order.getWhereLat());
        orderDto.setFromLong(order.getFromLong());
        orderDto.setWhereLong(order.getWhereLong());
        orderDto.setFromName(order.getFromName());
        orderDto.setWhereName(order.getWhereName());
        orderDto.setDistance(order.getDistance());
        orderDto.setCost(order.getCost());
        orderDto.setDate(order.getDate());
        orderDto.setTime(order.getTime());
        orderDto.setStatus(order.getStatus());
        orderDto.setRating(order.getRating());

        if (order.getPassenger() != null) {
            UserDto passengerDto = new UserDto(null, order.getPassenger().getUsername(), order.getPassenger().getFirstname(), order.getPassenger().getLastname(), null, null, null, true, null, null, null, null);
            orderDto.setPassenger(passengerDto);
        }
        if (order.getDriver() != null) {
            DriverDto driver = null;
            if (order.getDriver().getDriver() != null) {
                driver = new DriverDto();
                if (order.getDriver().getDriver().getDriversPhoto() != null) {
                    driver.setDriversPhoto(order.getDriver().getDriver().getDriversPhoto().getId());
                }
            }
            UserDto driverDto = new UserDto(null, order.getDriver().getUsername(), order.getDriver().getFirstname(), order.getDriver().getLastname(), null, null, null, true, null, driver, null, null);
            orderDto.setDriver(driverDto);
        }
        logger.info("Converted DTO object: " + orderDto);
        Resource<OrderDto> orderResource = new Resource<>(orderDto, linkTo(methodOn(OrderController.class).one(order.getId())).withSelfRel());
        if (orderDto.getStatus() == OrderStatus.IN_PROGRESS) {
            logger.info("Order status is: " + orderDto.getStatus());
            try {
                if (authorities.contains(AuthorityName.ROLE_DRIVER)) {
                    orderResource.add(linkTo(methodOn(OrderController.class).accept(order.getId())).withRel("accept"));
                    logger.info("Added possible action for the driver: accept");
                }
                if (authorities.contains(AuthorityName.ROLE_PASSENGER)) {
                    orderResource.add(linkTo(methodOn(OrderController.class).cancel(order.getId())).withRel("cancel"));
                    logger.info("Added possible action for the passenger: cancel");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (orderDto.getStatus() == OrderStatus.ACCEPTED) {
            logger.info("Order status is: " + orderDto.getStatus());
            try {
                if (authorities.contains(AuthorityName.ROLE_DRIVER)) {
                    orderResource.add(linkTo(methodOn(OrderController.class).start(order.getId())).withRel("start"));
                    orderResource.add(linkTo(methodOn(OrderController.class).terminate(order.getId())).withRel("terminate"));
                    logger.info("Added possible action for the driver: start, terminate");
                }
                if (authorities.contains(AuthorityName.ROLE_PASSENGER)) {
                    orderResource.add(linkTo(methodOn(OrderController.class).cancel(order.getId())).withRel("cancel"));
                    logger.info("Added possible action for the passenger: cancel");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (orderDto.getStatus() == OrderStatus.STARTED) {
            logger.info("Order status is: " + orderDto.getStatus());
            try {
                if (authorities.contains(AuthorityName.ROLE_DRIVER)) {
                    orderResource.add(linkTo(methodOn(OrderController.class).complete(order.getId())).withRel("complete"));
                    orderResource.add(linkTo(methodOn(OrderController.class).terminate(order.getId())).withRel("terminate"));
                    logger.info("Added possible action for the driver: complete, terminate");
                }
                if (authorities.contains(AuthorityName.ROLE_PASSENGER)) {
                    orderResource.add(linkTo(methodOn(OrderController.class).cancel(order.getId())).withRel("cancel"));
                    logger.info("Added possible action for the passenger: cancel");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (orderDto.getStatus() == OrderStatus.COMPLETED && authorities.contains(AuthorityName.ROLE_PASSENGER)) {
            logger.info("Order status is: " + orderDto.getStatus());
            try {
                orderResource.add(linkTo(methodOn(OrderController.class).pay(order.getId())).withRel("payment"));
                logger.info("Added possible action for the passenger: payment");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (orderDto.getStatus() == OrderStatus.COMPLETED && authorities.contains(AuthorityName.ROLE_DRIVER)) {
            try {
                orderResource.add(linkTo(methodOn(OrderController.class).terminate(order.getId())).withRel("terminate"));
                logger.info("Added possible action for the driver: terminate");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return orderResource;
    }
}