package com.sda.practical.service.impl;

import com.sda.practical.exception.ExceptionText;
import com.sda.practical.exception.GeneralException;
import com.sda.practical.model.DBFile;
import com.sda.practical.repository.DBFileRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

/**
 * Service class to handle user profile picture files
 */
@Slf4j
@Service
public class DBFileStorageService {

    private final Logger logger = LoggerFactory.getLogger(DBFileStorageService.class);

    @Autowired
    private DBFileRepository dbFileRepository;

    /**
     * Saving a file with validation of the name
     *
     * @param file driver's picture as MultipartFile
     * @return return saved DBFile
     * @throws GeneralException if chars are incorrect or cannot be stored
     */
    public DBFile storeFile(MultipartFile file) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        logger.info("Normalized file name: " + fileName);
        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new GeneralException(ExceptionText.INVALID_CHAR, fileName);
            }
            logger.info("File doesn't contain invalid characters");

            DBFile dbFile = new DBFile(fileName, file.getContentType(), file.getBytes());
            logger.info("Made a DBFile instance: " + dbFile);

            return dbFileRepository.save(dbFile);
        } catch (Exception ex) {
            throw new GeneralException(ExceptionText.CANT_STORE_FILE, fileName);
        }
    }

    /**
     * Saving a file with validation of the name
     *
     * @param fileId Url string
     * @return return DBFile
     * @throws GeneralException if file is not found
     */
    public DBFile getFile(String fileId) {
        logger.info("Getting file by id: " + fileId);
        return dbFileRepository.findById(fileId).orElseThrow(() -> new GeneralException(ExceptionText.FILE_NOT_FOUND, fileId));
    }
}
