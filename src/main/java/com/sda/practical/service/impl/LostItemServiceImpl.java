package com.sda.practical.service.impl;

import com.sda.practical.controller.LostItemController;
import com.sda.practical.dto.LostItemDto;
import com.sda.practical.model.LostItem;
import com.sda.practical.model.Order;
import com.sda.practical.model.utils.LostItemStatus;
import com.sda.practical.repository.LostItemRepository;
import com.sda.practical.service.LostItemService;
import com.sda.practical.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Service class to handling interactions with passenger repository
 */
@Slf4j
@Service
public class LostItemServiceImpl implements LostItemService {

    private final LostItemRepository lostItemRepository;
    private final ModelMapper modelMapper;
    private final OrderService orderService;
    private final Logger logger = LoggerFactory.getLogger(LostItemServiceImpl.class);


    @Autowired
    public LostItemServiceImpl(LostItemRepository lostItemRepository, ModelMapper modelMapper, OrderService orderService) {
        this.lostItemRepository = lostItemRepository;
        this.modelMapper = modelMapper;
        this.orderService = orderService;
    }

    @Override
    public Resources<LostItemDto> getListOfLostItems() {
        List<LostItemDto> list = lostItemRepository.findAll().stream().map(this::convertToDto).collect(Collectors.toList()); logger.info("Got list of lost items: " + list);
        return new Resources<>(list, linkTo(methodOn(LostItemController.class).getListOfLostItems()).withSelfRel());
    }

    @Override
    public LostItemDto submitLostItem(LostItemDto lostItemDto) {
        LostItem lostItem = modelMapper.map(lostItemDto, LostItem.class);
        lostItem.setStatus(LostItemStatus.SUBMITTED);
        logger.info("Submitted a lost item: " + lostItem);
        Order order = orderService.getOrderById(lostItemDto.getOrder());
        lostItem.setOrder(order);
        LostItem savedItem = lostItemRepository.save(lostItem);
        return convertToDto(savedItem);
    }

    @Override
    public LostItemDto getOne(Long id) {
        LostItem lostItem = lostItemRepository.getById(id);
        return convertToDto(lostItem);
    }

    @Override
    public LostItemDto close(Long id) {
        LostItem item = lostItemRepository.getById(id);
        item.setStatus(LostItemStatus.CLOSED);
        LostItem savedItem = lostItemRepository.save(item);
        logger.info("Closed a lost item case: " + savedItem);
        return convertToDto(savedItem);
    }

    private LostItemDto convertToDto(LostItem lostItem){
        LostItemDto lostItemDto = new LostItemDto();
        if(lostItem!=null) {
            lostItemDto.setId(lostItem.getId());
            lostItemDto.setDescription(lostItem.getDescription());
            lostItemDto.setType(lostItem.getType());
            lostItemDto.setStatus(lostItem.getStatus());
            lostItemDto.setOrder(lostItem.getOrder().getId());
        }
        return lostItemDto;
    }
}