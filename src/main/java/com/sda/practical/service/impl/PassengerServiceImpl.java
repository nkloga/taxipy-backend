package com.sda.practical.service.impl;

import com.sda.practical.exception.ExceptionText;
import com.sda.practical.exception.GeneralException;
import com.sda.practical.model.Passenger;
import com.sda.practical.repository.PassengerRepository;
import com.sda.practical.service.PassengerService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class to handling interactions with passenger repository
 */
@Slf4j
@Service
public class PassengerServiceImpl implements PassengerService {

    private final PassengerRepository passengerRepository;
    private final Logger logger = LoggerFactory.getLogger(PassengerServiceImpl.class);

    @Autowired
    public PassengerServiceImpl(PassengerRepository passengerRepository) {
        this.passengerRepository = passengerRepository;
    }

    @Override
    public Passenger savePassenger(Passenger passenger) {
        logger.info("Saving passenger: " + passenger);
        Passenger passengerInDb = passengerRepository.findByCCNumber(passenger.getCreditCardNumber());
        if (passengerInDb == null) {
            logger.info("There is no such passenger in the db");
            return passengerRepository.save(passenger);
        } else throw new GeneralException(ExceptionText.C_CARD_EXISTS);
    }

    @Override
    public Passenger updatePassenger(Passenger passenger) {
        logger.info("Updating passenger profile: " + passenger);
        Integer confirmation = passengerRepository.updatePassenger(passenger.getId(), passenger.getLocation(), passenger.getCreditCardNumber(), passenger.getCcv(), passenger.getExpirationDate(), passenger.getCountry());
        if (confirmation == 0){
            throw new GeneralException(ExceptionText.PROFILE_COULDNT_BE_UPDATED);
        }
        return passengerRepository.getOne(passenger.getId());
    }

    @Override
    public Passenger getOne(Long id) {
        logger.info("Getting passenger by id: " + id);
        return passengerRepository.findById(id).orElse(null);
    }
}

