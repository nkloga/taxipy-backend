package com.sda.practical.service.impl;

import com.sda.practical.exception.ExceptionText;
import com.sda.practical.exception.GeneralException;
import com.sda.practical.model.Driver;
import com.sda.practical.repository.DriverRepository;
import com.sda.practical.service.DriverService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class to handling interactions with driver repository
 */
@Slf4j
@Service
public class DriverServiceImpl implements DriverService {

    private final DriverRepository driverRepository;
    private final Logger logger = LoggerFactory.getLogger(DriverServiceImpl.class);

    @Autowired
    public DriverServiceImpl(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }


    @Override
    public Driver saveDriver(Driver driver) {
        Driver sameDriverInDb = driverRepository.findByLicenseNumber(driver.getLicenseNumber());
        logger.info("Saving driver: " + driver);
        if (sameDriverInDb == null) {
            return driverRepository.save(driver);
        } else throw new GeneralException(ExceptionText.D_LICENSE_EXISTS);
    }

    @Override
    public Driver updateDriver(Driver driver) {
        logger.info("Updating driver: " + driver);
        Integer state = driverRepository.updateDriver(driver.getId(), driver.getLicenseNumber(), driver.getCarPlateNumber(), driver.getCarManufacturingYear(), driver.getCarModel(), driver.getAmountOfSeats(), driver.getCarColor());
        if (state != 1) {
            throw new GeneralException(ExceptionText.PROFILE_COULDNT_BE_UPDATED);
        }
        return driverRepository.getOne(driver.getId());
    }

    @Override
    public Driver getOne(Long id) {

        logger.info("Getting driver by id: " + id);
        return driverRepository.findById(id).orElse(null);
    }
}
