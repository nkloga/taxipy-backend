package com.sda.practical.service.impl;

import com.sda.practical.dto.AuthUserDto;
import com.sda.practical.exception.ExceptionText;
import com.sda.practical.exception.GeneralException;
import com.sda.practical.model.*;
import com.sda.practical.dto.OrderDto;
import com.sda.practical.model.Order;
import com.sda.practical.model.utils.AuthorityName;
import com.sda.practical.service.order.OrderResourceAssembler;
import com.sda.practical.service.order.OrderValidator;
import com.sda.practical.repository.OrderRepository;
import com.sda.practical.model.utils.OrderStatus;
import com.sda.practical.controller.OrderController;
import com.sda.practical.service.OrderService;
import com.sda.practical.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;

import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Service class implementing all order handling related methods
 */
@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final OrderResourceAssembler orderResourceAssembler;
    private final UserService userService;
    private final ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, OrderResourceAssembler orderResourceAssembler, UserService userService, ModelMapper modelMapper) {
        this.orderRepository = orderRepository;
        this.orderResourceAssembler = orderResourceAssembler;
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @Override
    public Resource<OrderDto> add(OrderDto orderDto) {
        logger.info("Adding new order: " + orderDto);
        DataBinder binder = new DataBinder(orderDto);
        binder.addValidators(new OrderValidator());
        binder.validate();
        if (binder.getBindingResult().hasErrors()) throw new GeneralException(ExceptionText.ORDER_INVALID);
        logger.info("Order passed validation");
        AuthUserDto authUserDto = userService.authenticate();
        User passenger = userService.findByUserName(authUserDto.getUsername());
        logger.info("Got passenger profile: " + passenger);
        Order order = Order.of(orderDto.getFromLat(), orderDto.getWhereLat(), orderDto.getFromName(), orderDto.getFromLong(), orderDto.getWhereLong(), orderDto.getWhereName(), null, passenger, orderDto.getCost(), orderDto.getDistance(), orderDto.getTime());
        Order newOrder = orderRepository.save(order);
        return orderResourceAssembler.toResource(newOrder);
    }

    @Override
    public Resource<OrderDto> getOrderDtoById(Long id) {
        Order order = orderRepository.findById(id).orElseThrow(() -> new GeneralException(ExceptionText.ORDER_NOT_FOUND));
        logger.info("Got order: " + order + " by id: " + id);
        return orderResourceAssembler.toResource(order);
    }

    @Override
    public Order getOrderById(Long id) {
        return orderRepository.findById(id).orElseThrow(() -> new GeneralException(ExceptionText.ORDER_NOT_FOUND));
    }

    private Order changeOrderCostOnCancel(Order order) {
        if (order.getStatus() == OrderStatus.IN_PROGRESS && order.getDriver() == null) {
            order.setCost(0.0);
            logger.info("Order has not been accepted yet, set 0 cost");
        } else if (order.getStatus() == OrderStatus.ACCEPTED || order.getStatus() == OrderStatus.STARTED) {
            order.setCost(order.getCost() * 0.5);
            logger.info("Passenger is attempting to cancel the order after it has been accepted, set ride cost to 50% of estimation: " + order.getCost());
        }
        return order;
    }

    @Override
    public Resource<OrderDto> cancel(Long id) {
        AuthUserDto authUserDto = userService.authenticate();
        logger.info("Cancelling an order by id: " + id);
        Order order = findOrderById(id);
        checkIfOrderBelongsToUser(order);
        changeOrderCostOnCancel(order);
        if (authUserDto.getAuthority().equals("[ROLE_PASSENGER]") && (order.getStatus() == OrderStatus.IN_PROGRESS || order.getStatus() == OrderStatus.ACCEPTED || order.getStatus() == OrderStatus.STARTED)) {
            order.setStatus(OrderStatus.CANCELLED);
            logger.info("Order status has been set to Cancelled");
        } else {
            throw new GeneralException(ExceptionText.CANT_CANCEL_ORDER, order.getStatus().toString());
        }
        return orderResourceAssembler.toResource(orderRepository.save(order));
    }

    private void checkIfOrderBelongsToUser(Order order) {
        AuthUserDto authUserDto = userService.authenticate();
        boolean result = false;
        if (authUserDto.getAuthority().equals("[ROLE_PASSENGER]")) {
            if (order.getPassenger().getId().equals(authUserDto.getId())) {
                result = true;
            }
        } else if (authUserDto.getAuthority().equals("[ROLE_DRIVER]")) {
            if (order.getDriver().getId().equals(authUserDto.getId())) {
                result = true;
            }
        }
        if(!result){
            throw new GeneralException(ExceptionText.ORDER_HAS_NO_USER);
        }
    }

    @Override
    public Resource<OrderDto> complete(Long id) {
        logger.info("Completing an order by id: " + id);
        Order order = findOrderById(id);
        logger.info("Found order: " + order);
        checkIfOrderBelongsToUser(order);
        if (order.getStatus() != OrderStatus.STARTED) {
            throw new GeneralException(ExceptionText.CANT_COMPLETE_ORDER, order.getStatus().toString());
        }
        order.setStatus(OrderStatus.COMPLETED);
        order.setTime(new Date().getTime() - order.getDate().getTime());
        logger.info("Set order status to COMPLETED and set elapsed time: " + order.getTime());
        return orderResourceAssembler.toResource(orderRepository.save(order));
    }

    @Override
    public Resource<OrderDto> accept(Long id) {
        logger.info("Accepting an order by id: " + id);
        Order order = findOrderById(id);
        logger.info("Found order: " + order);
        if (order.getStatus() != OrderStatus.IN_PROGRESS || order.getDriver() != null) {
            throw new GeneralException(ExceptionText.CANT_ACCEPT_ORDER, order.getStatus().toString());
        }
        order.setStatus(OrderStatus.ACCEPTED);
        User loggedInUser = userService.findByUserName(userService.authenticate().getUsername());
        order.setDriver(loggedInUser);
        logger.info("Set order status to " + order.getStatus() + " and set driver to: " + order.getDriver());
        return orderResourceAssembler.toResource(orderRepository.save(order));
    }

    @Override
    public Resource<OrderDto> start(Long id) {
        logger.info("Starting an order by id: " + id);
        Order order = findOrderById(id);
        logger.info("Found order: " + order);
        checkIfOrderBelongsToUser(order);
        if (order.getStatus() != OrderStatus.ACCEPTED) {
            throw new GeneralException(ExceptionText.CANT_START_ORDER, order.getStatus().toString());
        }
        order.setStatus(OrderStatus.STARTED);
        order.setTime(new Date().getTime());
        logger.info("Set order status to STARTED and set current time: " + order.getTime());
        return orderResourceAssembler.toResource(orderRepository.save(order));
    }

    @Override
    public Resource<OrderDto> terminate(Long id) {
        logger.info("Terminating an order by id: " + id);
        Order order = findOrderById(id);
        logger.info("Found order: " + order);
        checkIfOrderBelongsToUser(order);
        if (order.getStatus() != OrderStatus.ACCEPTED && order.getStatus() != OrderStatus.COMPLETED && order.getStatus() != OrderStatus.STARTED) {
            throw new GeneralException(ExceptionText.CANT_TERMINATE_ORDER, order.getStatus().toString());
        }
        order.setStatus(OrderStatus.TERMINATED);
        order.setCost(0.0);
        logger.info("Set order status to TERMINATED and set cost to: " + order.getCost());
        return orderResourceAssembler.toResource(orderRepository.save(order));
    }

    @Override
    public Resource<OrderDto> pay(Long id) {
        logger.info("Paying for an order by id: " + id);
        Order order = findOrderById(id);
        logger.info("Found order: " + order);
        checkIfOrderBelongsToUser(order);
        if (order.getStatus() != OrderStatus.COMPLETED) {
            throw new GeneralException(ExceptionText.CANT_PAY_ORDER, order.getStatus().toString());
        }
        order.setStatus(OrderStatus.PAID);
        logger.info("Set order status to PAID");
        return orderResourceAssembler.toResource(orderRepository.save(order));
    }

    @Override
    public Resource<OrderDto> feedback(Long id, Integer rating) {
        logger.info("Setting ride feedback: " + rating + " for the order id: " + id);
        Order order = findOrderById(id);
        checkIfOrderBelongsToUser(order);
        if (order == null) {
            throw new GeneralException(ExceptionText.ORDER_NOT_FOUND);
        }
        if (rating < 0 || rating > 7) {
            throw new GeneralException(ExceptionText.INCORRECT_RATING);
        }
        order.setRating(rating);
        Order savedOrder = orderRepository.save(order);
        logger.info("Saved order: " + order);
        return orderResourceAssembler.toResource(savedOrder);
    }

    @Override
    public Resources<Resource<OrderDto>> getAllOpenOrders() {
        List<Resource<OrderDto>> orders = orderRepository.findAll().stream().filter(order -> order.getStatus() == OrderStatus.IN_PROGRESS).map(orderResourceAssembler::toResource).collect(Collectors.toList());
        logger.info("Got list of open orders: " + orders);
        return new Resources<>(orders);
    }

    @Override
    public Resource<OrderDto> getLastOpenOrder() {
        AuthUserDto authUserDto = userService.authenticate();
        logger.info("Getting last open order");
        User user = userService.findByUserName(authUserDto.getUsername());
        logger.info("Found currently authenticated user: " + user);
        List<Order> orderList;
        Resource<OrderDto> exitingOrder = null;
        switch (authUserDto.getAuthority()) {
            case "[ROLE_PASSENGER]":
                orderList = orderRepository.findOpenPassengerOrders(user);
                if (orderList.size() == 1) {
                    exitingOrder = orderResourceAssembler.toResource(orderList.get(0));
                    logger.info("Checking if passenger has only one order, found: " + exitingOrder);
                }
                break;
            case "[ROLE_DRIVER]":
                orderList = orderRepository.findOpenDriverOrders(user);
                if (orderList.size() == 1) {
                    exitingOrder = orderResourceAssembler.toResource(orderList.get(0));
                    logger.info("Checking if driver has only one order, found: " + exitingOrder);
                }
                break;
            default:
                return null;
        }
        return exitingOrder;
    }

    @Override
    public Resources<Resource<OrderDto>> getAllHistoryOrders() {
        logger.info("Getting list of closed orders for currently authenticated user");
        AuthUserDto authUserDto = userService.authenticate();
        User user = null;
        try {
            user = userService.findByUserName(authUserDto.getUsername());
        } catch (NullPointerException exc) {
            logger.error("Can't get user by username: " + exc);
        }
        if (user == null) {
            throw new GeneralException(ExceptionText.USER_NOT_FOUND);
        }
        logger.info("Found currently authenticated user: " + user);
        Long id;
        List<Resource<OrderDto>> orders = new ArrayList<>();
        if (user.getPassenger() != null) {
            id = user.getId();
            List<Resource<OrderDto>> passengerOrders = orderRepository.findAllPassengerOrders(id).stream().map(orderResourceAssembler::toResource).collect(Collectors.toList());
            logger.info("Found list of orders: " + passengerOrders);
            if (!passengerOrders.isEmpty()) {
                orders.addAll(passengerOrders);
            }
        } else if (user.getDriver() != null) {
            id = user.getId();
//            id = user.getDriver().getId();
            List<Resource<OrderDto>> driverOrders = orderRepository.findAllDriverOrders(id).stream().map(orderResourceAssembler::toResource).collect(Collectors.toList());
            logger.info("Found list of orders: " + driverOrders);
            if (!driverOrders.isEmpty()) {
                orders.addAll(driverOrders);
            }
        } else if (user.getAuthorities().get(0).getAuthority()== AuthorityName.ROLE_ADMIN) {
            orders = orderRepository.findAll().stream().map(orderResourceAssembler::toResource).collect(Collectors.toList());
        }
        else
         {
            throw new GeneralException(ExceptionText.ORDER_NOT_FOUND);
        }
        return new Resources<>(orders, linkTo(methodOn(OrderController.class).getAllMyOrders()).withSelfRel());
    }

    private Order findOrderById(Long id) {
        Order order = orderRepository.findById(id).orElseThrow(() -> new GeneralException(ExceptionText.ORDER_NOT_FOUND));
        logger.info("Found order: " + order);
        return order;
    }
}
