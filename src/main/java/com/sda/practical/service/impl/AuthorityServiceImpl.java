package com.sda.practical.service.impl;

import com.sda.practical.dto.AuthorityDto;
import com.sda.practical.model.Authority;
import com.sda.practical.model.utils.AuthorityName;
import com.sda.practical.repository.AuthorityRepository;
import com.sda.practical.service.AuthorityService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

/**
 * Service class to handle user authority
 */
@Slf4j
@Service
public class AuthorityServiceImpl implements AuthorityService {

    private final AuthorityRepository authorityRepository;
    private final Logger logger = LoggerFactory.getLogger(AuthorityServiceImpl.class);


    @Autowired
    public AuthorityServiceImpl(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    @Override
    public AuthorityDto add(SimpleGrantedAuthority authority) {

        Authority authInDb = authorityRepository.findByName(AuthorityName.valueOf(authority.getAuthority()));
        logger.info("Found authority in db: " + authInDb);
        if (authInDb == null) {
            Authority auth = new Authority(AuthorityName.valueOf(authority.getAuthority()));
            authInDb = authorityRepository.save(auth);
        }
        AuthorityDto authorityDto = new AuthorityDto(authInDb.getAuthority());
        authorityDto.setId(authInDb.getId());
        return authorityDto;
    }

    @Override
    public AuthorityDto getAuthorityById(Long id) {
        Authority authority = authorityRepository.findById(id).filter(u -> u.getId().equals(id)).orElse(null);
        logger.info("Found authority by id: " + authority);

        if (authority != null) {
            return convertToDto(authority);
        } else return null;
    }

    @Override
    public AuthorityDto getAuthorityByName(AuthorityName name) {
        Authority authority = authorityRepository.findByName(name);
        logger.info("Found authority by name: " + authority);

        if (authority != null) {
            return convertToDto(authority);
        } else return null;
    }

    private AuthorityDto convertToDto(Authority authority) {
        AuthorityDto authorityDto = new AuthorityDto();
        authorityDto.setId(authority.getId());
        authorityDto.setAuthority(authority.getAuthority());
        return authorityDto;
    }

}
