package com.sda.practical.service.impl;

import com.sda.practical.dto.DriverDto;
import com.sda.practical.dto.PassengerDto;
import com.sda.practical.dto.UserDto;
import com.sda.practical.exception.ExceptionText;
import com.sda.practical.exception.GeneralException;
import com.sda.practical.dto.AuthUserDto;
import com.sda.practical.model.*;
import com.sda.practical.repository.UserRepository;
import com.sda.practical.security.JwtUserFactory;
import com.sda.practical.service.DriverService;
import com.sda.practical.service.PassengerService;
import com.sda.practical.service.AuthorityService;
import com.sda.practical.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service class implementing all user related methods
 */

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final DriverService driverService;
    private final AuthorityService authorityService;
    private final PassengerService passengerService;
    private final ModelMapper modelMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
    private final Logger logger = LoggerFactory.getLogger(PassengerServiceImpl.class);

    @Autowired
    public UserServiceImpl(UserRepository userRepository, DriverService driverService, AuthorityService authorityService, PassengerService passengerService, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.driverService = driverService;
        this.authorityService = authorityService;
        this.passengerService = passengerService;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<AuthUserDto> getListOfUsers() {
        logger.info("Getting list of users");
        List<User> listOfUsers = userRepository.findAll();
        logger.info("Got list of: " + listOfUsers);
        List<AuthUserDto> usersDto = listOfUsers.stream().filter(user -> !user.getAuthorities().get(0).getAuthority().toString().equals("ROLE_ADMIN")).map(user -> {
            AuthUserDto authUsernew = new AuthUserDto();
            authUsernew.setId(user.getId());
            authUsernew.setAuthority(user.getAuthorities().get(0).getAuthority().toString());
            authUsernew.setId(user.getId());
            authUsernew.setUsername(user.getUsername());
            authUsernew.setEnabled(user.getEnabled());
            authUsernew.setFirstname(user.getFirstname());
            authUsernew.setLastname(user.getLastname());
            authUsernew.setPhoneNumber(user.getPhoneNumber());
            return authUsernew;
        }).collect(Collectors.toList());
        logger.info("Converted users to DTO objects: " + usersDto);
        return usersDto;
    }

    @Override
    public boolean updateCurrentUser(User user) {
        logger.info("Updating current user");
        User loggedInUser = userRepository.findByUsername(user.getUsername());
        logger.info("Got currently authenticated user: " + loggedInUser);
        if (bCryptPasswordEncoder.matches(user.getPassword(), loggedInUser.getPassword())) {
            logger.info("Password matches with user record in the db");
            if (user.getDriver() != null) {
                user.getDriver().setId(loggedInUser.getDriver().getId());
                driverService.updateDriver(user.getDriver());
                logger.info("Updated a driver");
            }
            if (user.getPassenger() != null) {
                user.getPassenger().setId(loggedInUser.getPassenger().getId());
                passengerService.updatePassenger(user.getPassenger());
                logger.info("Updated a passenger");
            }
            userRepository.updateUser(loggedInUser.getId(), user.getEmail(), user.getPhoneNumber());
            logger.info("Updating a user");
        } else throw new GeneralException(ExceptionText.INCORRECT_PW);
        return true;
    }

    @Override
    public UserDto register(User user) {
        logger.info("Registering a new user, it can be a driver or passenger");
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        List<Authority> authorities = user.getAuthorities().stream().map(auth -> authorityService.add(new SimpleGrantedAuthority(auth.getAuthority().name()))).map(auth -> {
            Authority authority = new Authority();
            authority.setAuthority(auth.getAuthority());
            authority.setId(auth.getId());
            return authority;
        }).collect(Collectors.toList());
        user.setAuthorities(authorities);
        logger.info("Set authorities: " + authorities);
        if (userRepository.findByUsername(user.getUsername()) == null) {
            logger.info("No such user record in the db");
            if (userRepository.findByEmail(user.getEmail()) == null) {
                logger.info("No user with this email in the db");
                if (user.getDriver() != null) {
                    user.setDriver(driverService.saveDriver(user.getDriver()));
                    logger.info("User driver is not null, saving driver record: " + user.getDriver() + " to the db");
                }
                if (user.getPassenger() != null) {
                    user.setPassenger(passengerService.savePassenger(user.getPassenger()));
                    logger.info("User driver is not null, saving passenger record: " + user.getPassenger() + " to the db");
                }
                user.setEnabled(true);
                logger.info("Set user enabled true");
                User savedUser = null;
                try {
                    savedUser = userRepository.save(user);
                } catch (Exception exc) {
                    logger.error("Couldn't save a record: " + exc);
                }
                if (savedUser == null) {
                    throw new GeneralException(ExceptionText.USER_NOT_FOUND);
                }
                logger.info("Saved user record: " + savedUser);
                PassengerDto passenger = null;
                DriverDto driver = null;
                if (savedUser.getPassenger() != null) {
                    passenger = modelMapper.map(savedUser.getPassenger(), PassengerDto.class);
                } else if (savedUser.getDriver() != null) {
                    driver = modelMapper.map(savedUser.getDriver(), DriverDto.class);
                }

                UserDto userDto = new UserDto(savedUser.getId(), savedUser.getUsername(), savedUser.getFirstname(), savedUser.getLastname(), savedUser.getEmail(), savedUser.getPassword(), JwtUserFactory.mapToGrantedAuthorities(savedUser.getAuthorities()), savedUser.getEnabled(), savedUser.getLastPasswordResetDate(), driver, passenger, savedUser.getPhoneNumber());

                if (userDto.getPassenger() != null) {
                    userDto.getPassenger().setCreditCardNumber(null);
                    userDto.getPassenger().setCcv(null);
                    userDto.getPassenger().setExpirationDate(null);
                }
                logger.info("Set sensitive user info to null");
                return userDto;
            } else throw new GeneralException(ExceptionText.EMAIL_EXISTS, user.getEmail());
        } else throw new GeneralException(ExceptionText.USERNAME_EXISTS, user.getUsername());
    }

    @Override
    public User findByUserName(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public AuthUserDto authenticate() {
        AuthUserDto authUserDto = new AuthUserDto();
        String userName = "";
        try {
            userName = SecurityContextHolder.getContext().getAuthentication().getName();
            authUserDto.setUsername(userName);
            authUserDto.setAuthority(SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString());
        } catch (NullPointerException exc) {
            logger.error("Cannot get security context data: " + exc);
        }
        authUserDto.setId(userRepository.findByUsername(userName).getId());
        logger.info("Authenticating a current user: " + userName + ", result: " + authUserDto);
        return authUserDto;
    }

    @Override
    public User getCurrentUser() {
        logger.info("Getting a current user");
        AuthUserDto authUser = authenticate();
        User user = userRepository.findByUsername(authUser.getUsername());
        logger.info("Got user: " + user);
        return user;
    }

    @Override
    public AuthUserDto blockUser(Long id, boolean block) {
        logger.info("User with id: " + id + " will be blocked (true), unblocked (false): " + block);
        User user = userRepository.findById(id).orElse(null);
        logger.info("Found user: " + user);
        if (user != null) {
            user.setEnabled(block);
            userRepository.save(user);
            AuthUserDto userDto = modelMapper.map(user, AuthUserDto.class);
            userDto.setAuthority(user.getAuthorities().get(0).getAuthority().toString());
            logger.info("Returning saved user: " + userDto);
            return userDto;
        } else return null;
    }
}
