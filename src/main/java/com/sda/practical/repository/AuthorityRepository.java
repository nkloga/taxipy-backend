package com.sda.practical.repository;

import com.sda.practical.model.Authority;
import com.sda.practical.model.utils.AuthorityName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    @Query("FROM Authority WHERE authority=:authority")
    Authority findByName(@Param("authority") AuthorityName authority);
}