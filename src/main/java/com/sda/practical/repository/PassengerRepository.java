package com.sda.practical.repository;

import com.sda.practical.model.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface PassengerRepository extends JpaRepository<Passenger, Long> {
    @Query("FROM Passenger WHERE creditCardNumber=:creditCardNumber")
    Passenger findByCCNumber(@Param("creditCardNumber") String creditCardNumber);

    @Transactional
    @Modifying
    @Query("UPDATE Passenger SET " +
            "location=:location, " +
            "creditCardNumber=:creditCardNumber, " +
            "ccv=:ccv, " +
            "expirationDate=:expirationDate, " +
            "country=:country WHERE id=:id")
    Integer updatePassenger(@Param("id") Long id, @Param("location") String location, @Param("creditCardNumber") String creditCardNumber, @Param("ccv") Integer ccv, @Param("expirationDate") String expirationDate, @Param("country") String country);
}







