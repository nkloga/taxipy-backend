package com.sda.practical.repository;

import com.sda.practical.model.Order;
import com.sda.practical.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    @Query("FROM Order WHERE (status='0' OR status='2' OR status='3' OR status='5') AND passenger=:passenger")
    List<Order> findOpenPassengerOrders(@Param("passenger") User passenger);

    @Query("FROM Order WHERE (status='0' OR status='2' OR status='3' OR status='5') AND driver=:driver")
    List<Order> findOpenDriverOrders(@Param("driver") User driver);

    @Query("FROM Order WHERE DRIVER_ID=:id")
    List<Order> findAllDriverOrders(@Param("id") Long id);

    @Query("FROM Order WHERE Passenger_ID=:id")
    List<Order> findAllPassengerOrders(@Param("id") Long id);

    @Query("FROM Order WHERE driver=:driver")
    List<Order> findOrdersByLostItem(@Param("driver") User driver);

    @Query("FROM Order WHERE passenger=:passenger")
    List<Order> findOrdersByFoundItem(@Param("passenger") User passenger);
}


