package com.sda.practical.repository;

import com.sda.practical.model.Driver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface DriverRepository extends JpaRepository<Driver, Long> {

    @Query("FROM Driver WHERE licenseNumber=:licenseNumber")
    Driver findByLicenseNumber(@Param("licenseNumber") String licenseNumber);

    @Transactional
    @Modifying
    @Query("UPDATE Driver SET licenseNumber=:licenseNumber, " +
            "carPlateNumber=:carPlateNumber," +
            "carManufacturingYear=:carManufacturingYear," +
            "carModel=:carModel," +
            "amountOfSeats=:amountOfSeats," +
            "carColor=:carColor WHERE id=:id"
    )
    Integer updateDriver(@Param("id")Long id, @Param("licenseNumber") String licenseNumber,
                            @Param("carPlateNumber") String carPlateNumber,
                            @Param("carManufacturingYear") String carManufacturingYear,
                            @Param("carModel") String carModel,
                            @Param("amountOfSeats") Integer amountOfSeats,
                            @Param("carColor") String carColor);
}