package com.sda.practical.repository;

import com.sda.practical.model.LostItem;
import com.sda.practical.model.Order;
import com.sda.practical.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LostItemRepository extends JpaRepository<LostItem, Long> {

    //TODO quick fix as getOne returns LazyInitializationException
    @Query("FROM LostItem WHERE id=:id")
    LostItem getById(@Param("id") Long id);
}
