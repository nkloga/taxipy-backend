package com.sda.practical.repository;

import com.sda.practical.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    // TODO fetching whole table, * should be replaced
    @Query(value = "SELECT * " + "FROM User " + "INNER JOIN Driver ON User.driver_id=Driver.id ", nativeQuery = true)
    List<User> findListOfDrivers();

    @Query(value = "SELECT * " + "FROM User u " + "INNER JOIN Passenger p ON u.passenger_id=p.id;", nativeQuery = true)
    List<User> findListOfPassengers();

    @Query(value = "SELECT * " + "FROM user " + "INNER JOIN authority ON user.authority_id=authority.id " + "WHERE authority.name='admin';", nativeQuery = true)
    List<User> findListOfAdmins();

    @Query("FROM User WHERE userName=:username")
    User findByUsername(@Param("username") String username);

    @Query("FROM User WHERE email=:email")
    User findByEmail(@Param("email") String email);

    @Query("FROM User WHERE driver_id=:id")
    User getDriver(@Param("id") Long id);

    @Query("FROM User WHERE passenger_id=:id")
    User getPassenger(@Param("id") Long id);

    @Transactional
    @Modifying
    @Query("UPDATE User SET email=:email, phoneNumber=:phone WHERE id=:id")
    Integer updateUser(@Param("id") Long id, @Param("email") String email, @Param("phone") String phone);
}