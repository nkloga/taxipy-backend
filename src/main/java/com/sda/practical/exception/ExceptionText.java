package com.sda.practical.exception;

public enum ExceptionText {

    NO_PERMISSION("You have no permission to access this resource."),
    INCORRECT_PW("The password you have inserted is incorrect!"),
    C_CARD_EXISTS("Passenger with this credit card number is already registered"),
    D_LICENSE_EXISTS("Driver license number is already in use!"),
    INVALID_CHAR("Sorry! Filename contains invalid path sequence %s"),
    EMAIL_EXISTS("User with the email: %s is already registered!"),
    FILE_NOT_FOUND("File not found with id %s"),
    CANT_STORE_FILE("Could not store file %s Please try again!"),
    USERNAME_EXISTS("User name: %s already exists in the system!"),
    CANT_CANCEL_ORDER("Can't cancel the order! Incorrect order status: %s"),
    CANT_COMPLETE_ORDER("Can't complete the order! Incorrect order status: %s"),
    CANT_START_ORDER("Can't start the order! Incorrect order status: %s"),
    CANT_PAY_ORDER("Can't pay for the order! Incorrect order status: %s"),
    CANT_ACCEPT_ORDER("Can't accept the order! Incorrect order status: %s"),
    CANT_TERMINATE_ORDER("Can't terminate the order! Incorrect order status: %s"),
    ORDER_NOT_FOUND("Order is not found"),
    BAD_CREDENTIALS("Bad credentials!"),
    USER_DISABLED("User is disabled!"),
    USER_NOT_FOUND("User is not found!"),
    ORDER_INVALID("Order validation has failed"),
    ITEM_CANNOT_BE_SUBMITTED("Item cannot be submitted"),
    INCORRECT_RATING("Rating value is incorrect and cannot be assigned to the order"),
    ORDER_HAS_NO_USER("This user is not part of the order"),
    PROFILE_COULDNT_BE_UPDATED("Profile couldn't be updated");

    private String message;

    ExceptionText(String url) {
        this.message = url;
    }

    public String url() {
        return message;
    }
}
