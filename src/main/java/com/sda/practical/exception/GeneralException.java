package com.sda.practical.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class GeneralException extends RuntimeException {
    public GeneralException(ExceptionText message) {
        super(message.url());
    }

    public GeneralException(ExceptionText message, String text) {
        super(String.format(message.url(), text));
    }
}
