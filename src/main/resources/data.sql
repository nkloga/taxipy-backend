
MERGE INTO AUTHORITY KEY (id) VALUES (23,'ROLE_PASSENGER');
MERGE INTO AUTHORITY KEY (id) VALUES (24,'ROLE_DRIVER');
MERGE INTO AUTHORITY KEY (id) VALUES (25,'ROLE_ADMIN');

--Passenger with one order, testing lost item
INSERT INTO PASSENGER(id,  birthdate, ccv, country, credit_card_number, expiration_date, location)
VALUES(100, '01.01.2000', 123, 'Estonia', '1111222233334444', '01.01.2024', 'Tallinn');

INSERT INTO USER (id, email, enabled, firstname, lastname, password, phonenumber, username, passenger_id)
VALUES(101, 'asda@test8.ee', true, 'FirstName', 'LastName', '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', '123123123', 'passenger', 100);

INSERT INTO DRIVER (id,  amount_of_seats, car_color, car_manufacturing_year, car_model, car_plate_number, license_number, drivers_photo_id)
VALUES(102,  5, 'blue', '2013', 'VW', '432DFG', '111113', null);

INSERT INTO USER (id, email, enabled, firstname, lastname, password, phonenumber, username, driver_id)
VALUES(103, 'asdasd@test9.ee', true, 'FirstName', 'LastName', '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', '123123123', 'driver', 102);

INSERT INTO USER (id, email, enabled, firstname, lastname, password, phonenumber, username)
VALUES(104, 'asdasd@test6.ee', true, 'FirstName', 'LastName', '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', '123123123', 'admin');

INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (103,24);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (101,23);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (104,25);




